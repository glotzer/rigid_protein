#!/usr/bin/python
"""Process the input files as needed"""
import subprocess
import tempfile
import argparse
import os
import numpy as np
import pickle
import shutil

PDB_POS = "2B3P_A_min_RK_-1.34_-1.01_conserv.pdb"
PDB_NEG = "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb"
TOP_POS = "pos_top.itp"
TOP_NEG = "neg_top.itp"

def get_charges(top_file):
    """Function to extract charges from a GROMACS topology file"""
    # Now read the topology files to get charges
    with open(top_file) as f:
        # Skip to atoms
        while 'atom' not in f.readline():
            continue
        f.readline()
        f.readline()

        # Loop until hitting distance restraints
        counter = 0
        with tempfile.NamedTemporaryFile('w') as tmp:
            while True:
                line = f.readline()
                if 'distance_restraints' in line:
                    break
                if not 'residue' in line:
                    tmp.write(line)
            tmp.flush()
            charges = np.loadtxt(tmp.name, usecols=(6,))
    return charges

def eigen_sphere(verts):
    cov= np.cov(np.asarray(verts).transpose())
    w,v = np.linalg.eig(cov)
    idx = np.argsort(w)[::-1]
    extent = np.dot(v[idx[0]]/np.linalg.norm(v[idx[0]]),verts.transpose())
    min_pt = np.argmin(extent)
    max_pt = np.argmax(extent)
    return 0.5*(verts[max_pt]+verts[min_pt]),0.5*(extent[max_pt]-extent[min_pt])

def sphere_of_sphere_and_pt(c,r,p):
    d = p - c;
    dist2 = np.dot(d,d);
    if dist2 > r*r:
        dist = np.sqrt(dist2);
        new_r = (r + dist) * 0.5;
        k = (new_r - r) /dist
        return c+d*k,new_r
    else:
        return c,r

def ritter_eigen_sphere(verts):
    c,r = eigen_sphere(verts)
    for v in verts:
        c,r = sphere_of_sphere_and_pt(c,r,v)
    return c,r

def parse_triangulation(triangulated_file, leave = False, add_hydrophobic = True):
    """Extract vertices and faces from an msms triangulation and add hydrophobicities as desired"""
    faces_file = triangulated_file + '.face'
    verts_file = triangulated_file + '.vert'
    faces = np.loadtxt(faces_file, skiprows=3)[:,0:3] - 1 # make 0 indexed
    vertices = np.loadtxt(verts_file, skiprows=3, usecols=(0, 1, 2))/10 # Switch from angstroms to nanometers

    if add_hydrophobic:
        # Use Kyte Doolittle Kd values for hydrophobicity
        # https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/midas/hydrophob.html
        HYDROPHOBICITY = dict(ILE=4.5, VAL=4.2, LEU=3.8, PHE=2.8, CYS=2.5, MET=1.9, ALA=1.8, GLY=-0.4, THR=-0.7, SER=-0.8, TRP=-0.9,TYR=-1.3, PRO=-1.6, HIS=-3.2, GLU=-3.5, GLN=-3.5, ASP=-3.5, ASN=-3.5, LYS=-3.9, ARG=-4.5)

        # Extract the hydrophobicity values by parsing the file,
        # the residue name is in the ninth column, which must be split
        hydrophobicity_values = []
        try:
            with open(verts_file) as f:
                for l in f.readlines():
                    tokens = l.split()
                    if len(tokens) >= 10:
                        hydrophobicity_values.append(HYDROPHOBICITY[tokens[9].split('_')[1]])
        except FileNotFoundError:
            raise RuntimeError(
                    "No vertices file found corresponding to {}, there should be a file {}".format(
                triangulated_file, verts_file))

    # clean up triangulated files
    if not leave:
        os.remove(faces_file)
        os.remove(verts_file)

    if add_hydrophobic:
        return vertices, faces, hydrophobicity_values
    else:
        return vertices, faces


# Annoyingly with tempfile you have to keep the files open the whole time,
# so there's a lot of nested with statements
def main(pdb_file, leave = False, density = 1.0):
    cur_dir = os.getcwd()
    changed_dir = False
    # There is a more elegant way involving just putting files in the right place, but I'm too lazy for that right now.
    if cur_dir != os.path.dirname(os.path.realpath(__file__)):
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        changed_dir = True

    # Need to be in this directory to import this successfully
    # (not a clean solution, but fine for now)
    try:
        from scripts import pdb_tools
    except ModuleNotFoundError:
        import pdb_tools

    msms_folder = './msms_i86Linux2_2.6.1'

    # First parse out the HETATM and CONECT entries
    cleaned_file = tempfile.NamedTemporaryFile('w')
    with cleaned_file as f_clean:
        with open(pdb_file) as fr:
            for line in fr:
                if not 'HETATM' in line and not 'CONECT' in line:
                    f_clean.write(line)

        # Now run pdb_to_xyzrn
        xyrn_file = tempfile.NamedTemporaryFile()
        with xyrn_file as f_xyrn:
            subprocess.check_output("{} {} > {}".format(
                msms_folder + '/pdb_to_xyzrn', cleaned_file.name, f_xyrn.name
                ), shell = True)

            # Input the xyzrn file to msms
            triangulated_file = 'tri.txt'
            subprocess.check_output("{} -if {} -of {} -density {}".format(
                msms_folder + '/msms.i86Linux2.2.6.1', xyrn_file.name, triangulated_file, density
                ), shell = True)

    # Get faces and vertices from the triangulated file
    verts, faces, hvs = parse_triangulation(triangulated_file, leave=leave)

    # Center the vertices using the Ritter eigensphere    
    r_center, r_circ = ritter_eigen_sphere(verts)
    verts = verts - r_center

    # Also read and shift the atomic positions using
    # the Ritter eigensphere so that they can be
    # used for charge positions
    pdb = pdb_tools.pdb()
    pdb.read(pdb_file)
    atoms = np.array(pdb.pos)/10 # Switch angstroms to nm
    atoms -= r_center

    # Save the output
    output_folder = './triangulated_files'
    save_file = pdb_file + ".data." + str(density)
    with open(save_file, 'wb') as f:
        pickle.dump(dict(
            atoms = atoms,
            verts=verts,
            faces = faces,
            hvs = hvs),
            f)
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    shutil.move(save_file, os.path.join(output_folder, os.path.basename(save_file)))

    # Make sure to revert to original directory
    if changed_dir:
        os.chdir(cur_dir)

    return save_file, verts, faces, hvs

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Convert a pdb file into a polyhedral representation"
            )
    parser.add_argument(
            "pdb_file",
            type=str,
            help="The pdb file to process."
            )
    parser.add_argument(
            "-d", "--density",
            type=float,
            default=1.0,
            help="The density of the surface representation."
            )
    parser.add_argument(
            "-l", "--leave",
            action="store_true",
            help="If specified, intermediate files will be retained"
            )
    args = parser.parse_args()
    save_file, verts, faces, hvs = main(**vars(args))
