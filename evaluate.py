#!/usr/bin/python
from __future__ import division, print_function, absolute_import
import sys
from schwimmbad import MPIPool
import numpy as np

import signac

from operations import *

def main(pool):
    project = signac.get_project()

    root_name = 'protomer_1_alternating_head_tail'
    original_file = root_name + '.pdb'
    key = 'pdb_file'

    schema = project.detect_schema()
    filenames = list(list(schema[key].values())[0])
    capped_files = [fn for fn in filenames if (root_name in fn and fn != original_file)]

    jobs = project.find_jobs({key: {'$in': capped_files}})

    jobs = [job for job in jobs if job.document.get('evaluated', False)]
#    jobs = list(project.find_jobs(
#        {"pdb_file": {"$in": ["protomer_1_alternating_head_tail_pos_middle.pdb", "protomer_1_alternating_head_tail_neg_middle.pdb", "protomer_1_alternating_head_tail_pos_top_neg_bottom.pdb", "protomer_1_alternating_head_tail_pos_top_pos_bottom.pdb", "protomer_1_alternating_head_tail_neg_top_neg_bottom.pdb"]}}
#        ))

#    print([job.get_id() for job in jobs])
    print()
    print(len(jobs))
    print()
    sys.stdout.flush()

    try:
        pool.map(evaluate_energy, jobs)
    except Exception as e:
        print("Allowing exception {} through".format(e))
        pass

if __name__ == '__main__':

    pool = MPIPool()

    with pool:
        if not pool.is_master():
            pool.wait()
            sys.exit(0)
        else:
            main(pool)
