#!/bin/bash
#SBATCH --job-name="evaluate"
#SBATCH --nodes=48
#SBATCH --ntasks-per-node=1
#SBATCH -t 48:00:00
#SBATCH --partition=skx-normal

set -u
set -e
cd /work/05525/vramasub/stampede2/projects/protein_crystals/rigid_protein

export HOOMD_WALLTIME_STOP=$((`date +%s` + 172800 - 2 * 60))
export OMP_NUM_THREADS=48

OMP_NUM_THREADS=48 PYTHONPATH=/work/05525/vramasub/stampede2/local/hoomd-single/build/:$PYTHONPATH I_MPI_FABRICS=tcp ibrun python3 evaluate.py

wait
