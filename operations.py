#!/usr/bin/python
# py2 compatibility
from __future__ import division, print_function, absolute_import

"""This file encodes operations for the coarse-grained simulation of proteins
by using a rigid body to represent the protein body and point charges to
control the electrostatic interactions between the protein"""

"""What does the simulation have to do?
Construct a rigid protein structure, then add
the necessary enthalpic patches, then simulate
in order to see if the resulting structure falls
apart.

Operation 1: Structure initialization
The initial structure is the protomer structure.
That means that we need to not only coarse grain
the actual protein representation, we also need
to extract information about the position and
orientation of each protein in the protomer so
that the entire structure can be generated.
The candidate structures can probably still be
generated as is, but then they have to be
immediately coarse-grained in some formulaic way.
This coarse-graining has to address both the
protein shape and the charge distribution on the
surface. Ideally, it would be sufficiently general
so as to allow the addition of hydrophobic patches
as well so that we can test the role of
hydrophobicity that way, although we are likely to
try and use the Gibbs Sampling approach.

Operation 2: Simulate
The simulation protocol needs to find the electrostatic
patches determined by the coarse-graining procedure and
attach the appropriate potentials to them. No other
interaction is required for the initial stage (i.e. until
we introduce hydrophobicity). Then we simply simulate and
see if the system denatures.
"""

import contextlib
import pickle
import numpy as np
import pandas as pd
import math
import sys
from scripts import pdb_tools
from scripts.utilities import conj, rotate, quat_mult, quaternion_from_matrix
import gsd.hoomd
import argparse
import signac
import hoomd
#hoomd._hoomd.set_num_threads(48)
import os
import shutil
from hoomd import hpmc, deprecated, jit
import tempfile
import datetime
from ConfigFiles import pdb_to_shape
import rowan

# global constants
NUM_STEPS = 1e5
COARSE_STRUCTURE_FILE = 'init.gsd'
PDB_COMPOSITE = 'proposed_protomer.pdb'

project = signac.get_project()
CONFIG_DIR = project.fn("ConfigFiles/")
del project
#PDB_POS = os.path.join(CONFIG_DIR, "2B3P_A_min_RK_-1.34_-1.01_conserv.pdb")
#PDB_NEG = os.path.join(CONFIG_DIR, "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb")

TYPE_MAP = dict(
    POS_TYPE=dict(
        name="GFP_Pos",
        id=0
        ),
    NEG_TYPE=dict(
        name="GFP_Neg",
        id=1
        ),
    )

def frame_centroid_MSD(positions, box):
    """Compute the MSD of particles relative to the centroid.
    This method accounts for box periodicity and the minimum image convention
    It will be used to dynamically stop jobs that have denatured already.

    Args:
        positions(np.array): A position array of shape (particles, coordinates)
        box (system.box): The box"""
    positions = positions.copy()

    positions[:, 0] = np.mod(positions[:, 0]+box.Lx/2, box.Lx)
    positions[:, 1] = np.mod(positions[:, 1]+box.Ly/2, box.Ly)
    positions[:, 2] = np.mod(positions[:, 2]+box.Lz/2, box.Lz)
    positions[:, 0] = np.mod(positions[:, 0]+box.Lx/2, box.Lx)
    positions[:, 1] = np.mod(positions[:, 1]+box.Ly/2, box.Ly)
    positions[:, 2] = np.mod(positions[:, 2]+box.Lz/2, box.Lz)

    # To apply minimum image convention, just choose one of the particles
    # as the basis and find the closest images of everything to that one
    basis_particle = positions[np.newaxis, 0, :]
    vectors = positions-basis_particle
    positions[:, 0] = np.where(np.abs(vectors[:, 0]) > box.Lx/2,
                                box.Lx - np.abs(positions[:, 0]),
                                positions[:, 0])
    positions[:, 1] = np.where(np.abs(vectors[:, 1]) > box.Ly/2,
                                box.Ly - np.abs(positions[:, 1]),
                                positions[:, 1])
    positions[:, 2] = np.where(np.abs(vectors[:, 2]) > box.Lz/2,
                                box.Lz - np.abs(positions[:, 2]),
                                positions[:, 2])

    # Now compute the centroids for each step and find deviations
    centroids = np.mean(positions, axis = 0)
    centroids = centroids[np.newaxis, :]
    deltas = positions - centroids
    distances = np.linalg.norm(deltas, axis = -1)
    avg_distances = np.mean(distances, axis = -1)
    return avg_distances

@contextlib.contextmanager
def _redirect_hoomd_log(job):
    """Simple function to redirect HOOMD log output"""
    log_file = job.fn('hoomd_{}.log'.format(hoomd.comm.get_partition()))
    tmp_log_file = log_file + '.tmp'
    if os.path.isfile(log_file):
        with open(tmp_log_file, 'a') as fw:
            with open(log_file) as fr:
                fw.write(fr.read())
                fw.write("Starting new run at {}\n".format(datetime.datetime.now()))
    else:
        # This is the first time
        with open(tmp_log_file, 'a') as fw:
            fw.write("Starting new run at {}\n".format(datetime.datetime.now()))
    hoomd.option.set_msg_file(log_file)
    yield
    with open(tmp_log_file, 'a') as fw:
        with open(log_file) as fr:
            fw.write(fr.read())
    shutil.move(tmp_log_file, log_file)
    hoomd.option.set_msg_file(None)

def get_charges(top_file, kappa=None):
    """Function to extract charges from a GROMACS topology file

    Args:
        kappa (float): The kappa value used to to shift the charges by a factor
            of :math:`\\frac{\\exp(\\kappa a)}{1+\\kappa a}` where :math:`a` is
            the radius of a given particle. The radii are pulled from the
            nonbonded.itp OPLS force field parameter file, specifically the
            :math:`sigma` value therein, which is double the van der Waals
            radius. If kappa is None no shift is applied. Note that strictly
            speaking the charge does not actually get shifted, we are just
            conveniently adding a prefactor into the charge so that we don't
            have to address it separately.

    Returns: The charges from the topology file, shifted as needed.
    """
    # Now read the topology files to get charges

    with open(top_file) as f:
        # Skip to atoms
        while 'atom' not in f.readline():
            continue
        f.readline()
        f.readline()

        # Loop until hitting distance restraints
        counter = 0
        with tempfile.NamedTemporaryFile('w') as tmp:
            while True:
                line = f.readline()
                if 'distance_restraints' in line or 'bonds' in line:
                    break
                if not 'residue' in line:
                    tmp.write(line)
            tmp.flush()

            # If we don't have a kappa value, we can just return directly
            if kappa is None:
                return np.loadtxt(tmp.name, usecols=(6,))
            else:
                # Use a pandas DataFrame for efficient joining
                charges = pd.read_table(tmp.name, header=None, usecols=[1, 6], delim_whitespace=True)

    charges.columns = ['name', 'charge']
    charges.set_index('name')

    # Read through the ffnonbonded file and parse out the lines to make a
    # readable file for pandas
    #NOTE: I'm assuming that the sigma values in the file are given in
    # nanometers, which seems correct given that they are on the order of 0.1
    # and the van der Waals radii of various atoms is typically on the order of
    # 100 pm
    with tempfile.NamedTemporaryFile() as tf:
        with open(os.path.join(CONFIG_DIR, "ffnonbonded.itp")) as f_ff:
            for line in f_ff:
                if line.startswith(' opls'):
                    if ';' in line:
                        tf.write((line[:line.find(';')] + '\n').encode())
                    else:
                        tf.write(line.encode())
        tf.seek(0)
        radii = pd.read_table(tf, names=['name', 'tmp0', 'tmp1', 'mass', 'charge', 'ptype', 'sigma', 'epsilon'], delim_whitespace=True, index_col=False, usecols=[0, 6])

    # Join the charges onto the radii, then multiply the appropriate factor
    radii.set_index('name')
    charges = charges.merge(radii, how='left')
    if charges.isna().any()['sigma']:
        raise ValueError("Some atom type is missing a radius in the opls nonbonded file")
    charges['r'] = charges['sigma']/2
    return charges.apply(lambda x: x['charge']*np.exp(kappa*x['r'])/(1+kappa*x['r']),
                         axis='columns').values

def gen_yukawa_potential(kappa):
    bjerrum_length = 0.7 # in nanometers
    yukawa = """float rsq = dot(r_ij, r_ij);
                float r_inv = fast::rsqrt(rsq);
                return {}*charge_i*charge_j*fast::exp(-{}/r_inv)*r_inv;""".format(
                        bjerrum_length, kappa)
    return yukawa

def assemble_structure(job):
    """This operation takes the pdb file for the job
    passed in and generates the rigid protomer structure
    to be used in coarse-grained simulations

    Args:
        job (signac.contrib.Job): The job to operate on
    """
    with job:
        if job.sp.pos_type == "GFP":
            PDB_POS = os.path.join(CONFIG_DIR, "2B3P_A_min_RK_-1.34_-1.01_conserv.pdb")
            PDB_NEG = os.path.join(CONFIG_DIR, "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb")
        elif job.sp.pos_type == "refined":
            PDB_POS = os.path.join(CONFIG_DIR, "pos_refined.pdb")
            PDB_NEG = os.path.join(CONFIG_DIR, "neg_refined.pdb")
        elif job.sp.pos_type == "Ceru":
            if job.sp.get('mutated_chain') is not None:
                PDB_POS = os.path.join(CONFIG_DIR, "Ceru_" + '_'.join(job.sp.mutation) + '.pdb')
                PDB_NEG = os.path.join(CONFIG_DIR, "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb")
            else:
                PDB_POS = os.path.join(CONFIG_DIR, "Ceru.pdb")
                PDB_NEG = os.path.join(CONFIG_DIR, "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb")
        else:
            raise ValueError("Invalid pos_type!")

        project = signac.get_project()

        pdb_assembly = pdb_tools.pdb()
        pdb_assembly.read(PDB_COMPOSITE)

        pdb_plus = pdb_tools.pdb()
        pdb_plus.read(PDB_POS)

        pdb_minus = pdb_tools.pdb()
        pdb_minus.read(PDB_NEG)

        atoms_plus = np.array(pdb_plus.pos)/10 # Switch angstroms to nm
        centroid_plus = np.mean(atoms_plus, axis=0)
        atoms_plus -= centroid_plus

        atoms_minus = np.array(pdb_minus.pos)/10 # Switch angstroms to nm
        centroid_minus = np.mean(atoms_minus, axis=0)
        atoms_minus -= centroid_minus

        if len(atoms_minus) == len(atoms_plus):
            raise RuntimeError(
            "This script requires the two constituent proteins to have different numbers \
                    of atoms to differentiate them")

        snap = gsd.hoomd.Snapshot()
        snap.particles.N = len(pdb_assembly.chain_ids)

        pos = []
        orientation = []

        # For attaching potentials later
        atom_pos = []

        offset = 0
        types = []
        for i, (chain_id, chain_atoms) in enumerate(
                zip(pdb_assembly.chain_ids, pdb_assembly.chain_atoms)):
            atoms = np.array(pdb_assembly.pos[offset:offset+len(chain_atoms)])/10 # Scale to nm
            offset += len(chain_atoms)
            centroid_B = np.mean(atoms,axis=0)

            is_plus = len(atoms) == len(atoms_plus)

            if is_plus:
                types.append(TYPE_MAP['POS_TYPE']['id'])
            else:
                types.append(TYPE_MAP['NEG_TYPE']['id'])

            centroid_A = np.zeros(3)
            if is_plus:
                AA = atoms_plus - np.tile(centroid_A, (len(atoms_plus),1))
            else:
                AA = atoms_minus - np.tile(centroid_A, (len(atoms_minus),1))

            BB = atoms - np.tile(centroid_B, (len(atoms), 1))

            H = np.dot(np.transpose(AA),BB)
            U, S, Vt = np.linalg.svd(H)
            R = np.dot(Vt.T,U.T)

            # special reflection case
            if np.linalg.det(R) < 0:
               Vt[2,:] *= -1
               R = np.dot(Vt.T, U.T)

            t = np.dot(-R,centroid_A.T) + centroid_B.T
            pos.append(t)
            orientation.append(quaternion_from_matrix(R))

        pos = np.array(pos)
        pos -= np.mean(pos,axis=0)
        snap.particles.position = pos
        snap.particles.orientation = np.array(orientation)
        snap.particles.typeid = np.array(types)
        # This is verbose but I'm terrified of messing this up when hardcoding
        snap.particles.types = [n[1] for n in sorted([(t['id'], t['name']) for t in TYPE_MAP.values()])]

        r_max = np.max(np.linalg.norm(pos,axis=1)) + np.max(np.linalg.norm(atoms_plus,axis=1))
        L = 2*r_max
        snap.configuration.box = np.array([L,L,L,0,0,0], dtype=np.float32)

        gsd.hoomd.create(COARSE_STRUCTURE_FILE, snap)

def evaluate_energy(job):
    """This operation takes the rigid protomer
    structure generated by assemble_protomer and
    runs a simulation to see if this structure remains
    stable if only electrostatics are enabled

    Args:
        job (signac.contrib.Job): The job to operate on
    """
    # Outer try catch ensures failed jobs don't occupy process forever.
    try:
        # To ensure no mistakes with schwimmbad, explicitly return here
#        if job.document.get('evaluated') is not None:
#            return
        # Initialize HOOMD if needed
        if hoomd.context.exec_conf is None:
            hoomd.context.initialize('--mode=cpu --nthreads=48')
        with job:
            with _redirect_hoomd_log(job):
            #if True:
                with hoomd.context.SimulationContext() as cont:
                    #job.document['schwimmbad'] = True
                    print("Starting with job {}".format(job))
                    sys.stdout.flush()
                    if job.sp.pos_type == "GFP":
                        PDB_POS = os.path.join(CONFIG_DIR, "2B3P_A_min_RK_-1.34_-1.01_conserv.pdb")
                        PDB_NEG = os.path.join(CONFIG_DIR, "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb")
                        DATA_POS = os.path.join(CONFIG_DIR, "triangulated_files", "2B3P_A_min_RK_-1.34_-1.01_conserv.pdb.data." + str(job.sp.repr_density))
                        DATA_NEG = os.path.join(CONFIG_DIR, "triangulated_files", "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb.data." + str(job.sp.repr_density))
                    elif job.sp.pos_type == "refined":
                        PDB_POS = os.path.join(CONFIG_DIR, "pos_refined.pdb")
                        PDB_NEG = os.path.join(CONFIG_DIR, "neg_refined.pdb")
                        DATA_POS = os.path.join(CONFIG_DIR,
                                "triangulated_files", "pos_refined.pdb.data." + str(job.sp.repr_density))
                        DATA_NEG = os.path.join(CONFIG_DIR,
                                "triangulated_files", "neg_refined.pdb.data." + str(job.sp.repr_density))
                    elif job.sp.pos_type == "Ceru":
                        if job.sp.get('mutated_chain') is not None:
                            PDB_POS = os.path.join(CONFIG_DIR, "Ceru_" + '_'.join(job.sp.mutation) + '.pdb')
                            PDB_NEG = os.path.join(CONFIG_DIR, "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb")
                            DATA_POS = os.path.join(CONFIG_DIR, "triangulated_files", "Ceru_" + '_'.join(job.sp.mutation) + '.pdb.data.'+ str(job.sp.repr_density))
                            DATA_NEG = os.path.join(CONFIG_DIR, "triangulated_files", "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb.data." + str(job.sp.repr_density))
                        else:
                            PDB_POS = os.path.join(CONFIG_DIR, "Ceru.pdb")
                            PDB_NEG = os.path.join(CONFIG_DIR, "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb")
                            DATA_POS = os.path.join(CONFIG_DIR, "triangulated_files", "Ceru.pdb.data." + str(job.sp.repr_density))
                            DATA_NEG = os.path.join(CONFIG_DIR, "triangulated_files", "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb.data." + str(job.sp.repr_density))
                    else:
                        raise ValueError("Invalid pos_type!")

                    # File name setup
                    fname_base = 'evaluate'
                    log_file = fname_base + '.log'
                    centroid_log_file = fname_base + '_centroid.log'
                    msd_file = fname_base + '_msd.log'
                    xml_file = fname_base + '_0.xml'
                    restart_file = fname_base + '_restart.gsd'
                    output_file = fname_base + '.gsd'
                    top_pos = job.fn("pos_top.itp")
                    top_neg = job.fn("neg_top.itp")
                    use_deps = job.sp.get('dep', False)
                    if 'negative_segments' in job.sp:
                        seed = np.mod(np.prod(job.sp.negative_segments), 7**4) + job.sp.run_num # Arbitrary way to deterministically construct the seed
                    else:
                        seed = int(job.sp.r_cut_factor**job.sp.run_num)

                    # Initialization and setup
                    system = hoomd.init.read_gsd(filename = COARSE_STRUCTURE_FILE,
                            restart = restart_file)
                    if use_deps:
                        depletant_type = "Depletant"
                        depletant_radius = 0.3 # water radius is approximately 3 angstroms
                        """
                        To calculate the water density:
                        1 g/cm^3 * ((100cm/1e9 nm)^3) * (1 mol / 18.015 g) * (6.02*10^23 molecules/mol)
                        """
                        dep_density = ((100.0/1e9)**3)*(1/18.015)*(6.02e23)

                        mc = hpmc.integrate.polyhedron(seed=int(seed), d=0, a=0, implicit=True, depletant_mode = 'overlap_regions')
                        system.particles.types.add(depletant_type)
                        mc.set_params(depletant_type=depletant_type)
                        mc.shape_param.set(depletant_type, vertices=[], faces=[],
                                           sweep_radius=depletant_radius)
                        mc.set_params(nR=0)
                    else:
                        mc = hpmc.integrate.polyhedron(seed=int(seed), d=0, a=0)

                    # Get face and vertex information
                    ###CONTROL: For the control runs to work, will need to comment out all the f2 related lines
                    try:
                        f1 = open(DATA_POS, 'rb')
                        f2 = open(DATA_NEG, 'rb')
                    except FileNotFoundError:
                        print("No shape data files found for this density, regenerating now")
                        pdb_to_shape.main(PDB_POS, density = job.sp.repr_density)
                        pdb_to_shape.main(PDB_NEG, density = job.sp.repr_density)

                        f1 = open(DATA_POS, 'rb')
                        f2 = open(DATA_NEG, 'rb')
                    finally:
                        pos_data = pickle.load(f1)
                        neg_data = pickle.load(f2)

                    mc.shape_param.set(TYPE_MAP["POS_TYPE"]['name'],
                            vertices = pos_data['verts'],
                            faces = pos_data['faces'])
                    mc.shape_param.set(TYPE_MAP["NEG_TYPE"]['name'],
                            vertices = neg_data['verts'],
                            faces = neg_data['faces'])
                    mc.set_params(d={TYPE_MAP["POS_TYPE"]['name']: 0.01, TYPE_MAP["NEG_TYPE"]['name']: 0.01},
                            a={TYPE_MAP["POS_TYPE"]['name']: 0.01, TYPE_MAP["NEG_TYPE"]['name']: 0.01})

                    log_quantities = ['time', 'hpmc_sweep', 'hpmc_overlap_count', 'hpmc_translate_acceptance', 'hpmc_rotate_acceptance', 'hpmc_d', 'hpmc_a', 'volume',
                            'hpmc_patch_energy']
                    centroid_log_quantities = ['time', 'hpmc_sweep', 'centroid_distance']

                    # Dump files
                    g_all = hoomd.group.all()
                    gsd = hoomd.dump.gsd(
                        filename=output_file, group=g_all, period=50, phase=0, dynamic=['property','momentum'])
                    restart = hoomd.dump.gsd(
                        filename=restart_file, group=g_all, period=50, phase=0, truncate=True)
                    log = hoomd.analyze.log(
                        filename=log_file, header_prefix='#', quantities=log_quantities, period=10)
                    if job.isfile(xml_file):
                        msd = deprecated.analyze.msd(filename=msd_file, groups=[g_all], period=1,
                                header_prefix='# ', r0_file=xml_file)
                    else:
                        msd = deprecated.analyze.msd(filename=msd_file, groups=[g_all], period=1,
                                header_prefix='# ')
                    msd.disable() # Not using right now

                    # One time dumps
                    ###CONTROL: For the control runs to work, will need to comment out either the positive or negative lines (depending on the control)
                    pos_atoms = pos_data['atoms']
                    neg_atoms = neg_data['atoms']
                    if job.sp.get('shift', False): # Multiply charges by the prefactor
                        charges_plus = get_charges(top_pos, job.sp.kappa)
                        charges_minus = get_charges(top_neg, job.sp.kappa)
                    else:
                        charges_plus = get_charges(top_pos)
                        charges_minus = get_charges(top_neg)

                    # Rescale to resolve overlaps
                    if job.document.get('evaluated_once') is None:
                        num_expansions = 0;
                        # Rescale box once without scaling particles to allow cluster moves
                        # (also for sims without cluster moves for comparability)
                        hoomd.update.box_resize(L = 4*system.box.Lx, period = None, scale_particles = False)

                        original_L = system.box.Lx
                        xml = deprecated.dump.xml(group=g_all, filename=xml_file,
                                all=True);

                        pos_file = fname_base + '.pos'
                        pos = deprecated.dump.pos(filename=pos_file, period=1)
                        mc.setup_pos_writer(pos,
                                colors={
                                    # Blueish for positive, red for negative
                                    TYPE_MAP["POS_TYPE"]['name']:'ff5984ff',
                                    TYPE_MAP["NEG_TYPE"]['name']:'ffff0000',
                                    })
                        hoomd.run(1)
                        pos.disable() # Only for shape information
                        while mc.count_overlaps() > 0:
                            num_expansions += 1
                            hoomd.context.msg.notice(1, 'There are {} overlaps in your system. Will resize for the {} time\n'.format(
                                mc.count_overlaps(), num_expansions))
                            if np.abs(system.box.Lx - system.box.Ly) > 1e-5 or np.abs(system.box.Lx != system.box.Lz) > 1e-5:
                                raise RuntimeError("Box is not cubic, box dimensions are ({}, {}, {}).".format(
                                    system.box.Lx, system.box.Ly, system.box.Lz))
                            hoomd.update.box_resize(L = 1.01*system.box.Lx, period = None)
                            hoomd.run(1)
                        job.doc.num_expansions_evaluate = num_expansions

                        # Increase rotation move size, but only for certain types of runs
                        if job.statepoint().get('low_rotation') is None:
                            mc.set_params(a=0.5)

                        pos.set_period(100)
                        log.set_period(100)

                        # Now attempt to compress the system back to its original size
                        from utils import Compressor
                        comp = Compressor.Compressor(system, mc)
                        pre_compression_step = hoomd.get_step()
                        try:
                            comp.compress_cubic(original_L, allow_translation=False)
                        except RuntimeError:
                            job.doc.forbidden = True
                            raise RuntimeError("This configuration appears geometrically infeasible since the molecular surface results in overlaps")
                        pos.set_period(1)
                        log.set_period(1)

                        post_compression_step = hoomd.get_step()
                        job.doc.evaluate_num_steps_compressed = post_compression_step - pre_compression_step
                        # Reset all move sizes
                        if use_deps:
                            mc.set_params(nR=dep_density)
                            mc.set_params(d=0.1, a=0.1)
                        else:
                            mc.set_params(d=0.1, a=0.1)

                        job.document['evaluated_once'] = True

                    # Setup patches
                    patch = jit.patch.user_union(mc,
                            r_cut=job.sp.r_cut_factor/job.sp.kappa,
                            code=gen_yukawa_potential(job.sp.kappa))
                    patch.set_params(TYPE_MAP['POS_TYPE']['name'],
                            positions=pos_atoms,
                            typeids=[TYPE_MAP['POS_TYPE']['id']]*len(pos_atoms),
                            charges=charges_plus)
                    patch.set_params(TYPE_MAP['NEG_TYPE']['name'],
                            positions=neg_atoms,
                            typeids=[TYPE_MAP['NEG_TYPE']['id']]*len(neg_atoms),
                            charges=charges_minus)

                    # Add cluster moves
                    if job.sp.use_clusters:
                        if 'positive_segments' in job.sp:
                            seed_clusters = np.mod(np.prod(
                                job.sp.positive_segments), 7**4) + job.sp.run_num # Random construction
                        else:
                            seed_clusters = int(job.sp.run_num**job.sp.r_cut_factor) # Random construction

                        clusters = hpmc.update.clusters(mc,seed=seed_clusters)

                    # Start logging the distances (we don't want to include compression)
                    centroid_log_period = 10
                    centroid_log = hoomd.analyze.log(
                        filename=centroid_log_file, header_prefix='#',
                        quantities=centroid_log_quantities,
                        period=centroid_log_period)
                    def callback_fun(timestep):
                        """Simple callback function to log the centroid distances"""
                        snap = system.take_snapshot()
                        return frame_centroid_MSD(snap.particles.position, snap.box)
                    centroid_log.register_callback( # Logger for centroid distances only
                            'centroid_distance',
                            callback_fun)

                    # Tune if this is the first time
                    tuner_period = 100
                    tuning_steps = 4
                    if job.document.get('evaluate_tuned') is None:
                        # Allow things to run by first randomizing the system and
                        # then tuning the step size
                        mc_tune = hpmc.util.tune(mc, tunables=['d', 'a'], max_val=[
                                                 0.5, 0.5], gamma=1, target=0.3)
                        for i in range(tuning_steps):
                            mc_tune.update()
                            hoomd.run(tuner_period)

                        if hoomd.comm.get_rank() == 0:
                            job.document['evaluate_tuned'] = True
                            job.document['evaluate_d_tuned'] = mc.get_d()
                            job.document['evaluate_a_tuned'] = mc.get_a()
                    else:
                        mc.set_params(d=job.document.get('evaluate_d_tuned'),
                                      a=job.document.get('evaluate_a_tuned'))

                    # Run until done
                    try:
                        runupto_val = NUM_STEPS + tuner_period * tuning_steps + job.doc.evaluate_num_steps_compressed + job.doc.num_expansions_evaluate + 1
                        # Run for some interval, then after that every time we run
                        # for the same interval we then check if any of the distances
                        # up to this point are large enough to represent the system
                        # falling apart. If so, terminate the run
                        interval_length = 100 # Need at least 100 data points
                        interval = interval_length*centroid_log_period
                        conf_int_mult = 3 # the confidence interval
                        absolute_cutoff = 20 # For simplicity, also include an absolute distance cutoff
                        while hoomd.get_step() < runupto_val:
                            if not job.document.get('denatured'):
                                # For restartability
                                hoomd.run(interval)

                            # First perform absolute check
                            if centroid_log.query('centroid_distance') > absolute_cutoff:
                                job.document['denatured'] = True
                            # If the absolute metric is not met, check the statistical one
                            else:
                                distances = np.genfromtxt(centroid_log_file, invalid_raise=False)[:, len(centroid_log_quantities)] # Off by one because timestep is alwaysincluded
                                for i in range(len(distances)-interval_length, len(distances)):
                                    m = np.mean(distances[:i])
                                    std = np.std(distances[:i])
                                    if distances[i] > m + conf_int_mult*std:
                                        job.document['denatured'] = True
                                        break

                            # If we're done, run a little longer to collect statistics, then terminate
                            if job.document.get('denatured') is not None:
                                hoomd.run(2*interval)
                                break
                        #hoomd.run_upto(runupto_val)
                        if hoomd.comm.get_rank() == 0:
                            # Store metadata
                            metadata = job.document.get('hoomd_metadata', dict())
                            metadata[str(datetime.datetime.now().timestamp())] = hoomd.meta.dump_metadata()
                            job.document['hoomd_metadata'] = metadata
                            job.document['evaluated'] = True
                            print("Finished with job {}".format(job))
                            sys.stdout.flush()
                    except hoomd.WalltimeLimitReached:
                        if hoomd.comm.get_rank() == 0:
                            job.document['num_steps'] = hoomd.get_step()
                            print("Finished with job {}".format(job))
                            sys.stdout.flush()
    #except Exception as e:
    except AssertionError as e:
        # To prevent hanging and wasting time with schwimmbad
        print("Allowing exception {} through".format(e))
        print("Finished with job {}".format(job))
        sys.stdout.flush()
        pass

def test_charge_placement(job):
    """This operation checks whether charges are placed
    appropriately. It does so by explicitly creating spherical
    particles to represent charge points and centering them on
    the protein.
    """
    # Initialize HOOMD if needed
    if hoomd.context.exec_conf is None:
        hoomd.context.initialize('--mode=cpu')
    with job:
        with hoomd.context.SimulationContext() as cont:
            typ = 'mixed' # hardcode the type for now

            # File name setup
            fname_base = typ + '_placement'
            log_file = fname_base + '.log'
            msd_file = fname_base + '_msd.log'
            xml_file = fname_base + '_0.xml'
            restart_file = fname_base + '_restart.gsd'
            output_file = fname_base + '.gsd'
            top_pos = job.fn("pos_top.itp")
            top_neg = job.fn("neg_top.itp")
            seed = np.mod(np.prod(job.sp.negative_segments), 7**4) # Random construction

            # Initialization and setup
            system = hoomd.init.create_lattice(
                    hoomd.lattice.sc(a=10, type_name=TYPE_MAP["POS_TYPE"]['name']), n=1)
            system.replicate(nx=2, ny=2, nz=2)

            # Just always add both for ease.
            [system.particles.types.add(TYPE_MAP[t]['name']) for t in ["POS_TYPE", "NEG_TYPE"]]

            # Add fake particles for charges
            system.particles.types.add("POS_CHARGE")
            system.particles.types.add("NEG_CHARGE")

            if typ in ['positive', 'negative']:
                type_to_use = "POS_TYPE" if typ == 'positive' else "NEG_TYPE"
                for p in system.particles:
                    p.type = TYPE_MAP[type_to_use]['name']
            elif typ == 'mixed':
                for i in range(len(system.particles)):
                    if i % 2:
                        system.particles[i].type = TYPE_MAP["POS_TYPE"]['name']
                    else:
                        system.particles[i].type = TYPE_MAP["NEG_TYPE"]['name']
            else:
                raise RuntimeError("Unknown simulation type!")

            mc_poly = hpmc.integrate.polyhedron(seed=int(seed), d = 0.1, a = 0.1)
            mc_sphere = hpmc.integrate.sphere(seed=int(seed), d = 0.1, a = 0.1)

            # Get face and vertex information
            DATA_POS = os.path.join(CONFIG_DIR, "triangulated_files", "2B3P_A_min_RK_-1.34_-1.01_conserv.pdb.data." + str(job.sp.repr_density))
            DATA_NEG = os.path.join(CONFIG_DIR, "triangulated_files", "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb.data." + str(job.sp.repr_density))
            try:
                f1 = open(DATA_POS, 'rb')
                f2 = open(DATA_NEG, 'rb')
            except FileNotFoundError:
                print("No shape data files found for this density, regenerating now")
                pdb_to_shape.main(PDB_POS, density = job.sp.repr_density)
                pdb_to_shape.main(PDB_NEG, density = job.sp.repr_density)
                f1 = open(DATA_POS, 'rb')
                f2 = open(DATA_NEG, 'rb')
            finally:
                pos_data = pickle.load(f1)
                neg_data = pickle.load(f2)

            original_particles = [system.particles[i] for i in range(len(system.particles))]
            for p_main in original_particles:
                if p_main.type == TYPE_MAP["POS_TYPE"]['name']:
                    for pos_i in pos_data['atoms']:
                        t = system.particles.add("POS_CHARGE")
                        p = system.particles.get(t)
                        p.position = p_main.position + pos_i
                else:
                    for neg_i in neg_data['atoms']:
                        t = system.particles.add("NEG_CHARGE")
                        p = system.particles.get(t)
                        p.position = p_main.position + neg_i
            mc_poly.shape_param.set("POS_CHARGE", vertices = [], faces = [])
            mc_poly.shape_param.set("NEG_CHARGE", vertices = [], faces = [])
            mc_poly.shape_param.set(TYPE_MAP["POS_TYPE"]['name'],
                    vertices = pos_data['verts'],
                    faces = pos_data['faces'])
            mc_poly.shape_param.set(TYPE_MAP["NEG_TYPE"]['name'],
                    vertices = neg_data['verts'],
                    faces = neg_data['faces'])
            mc_poly.set_params(d={TYPE_MAP["POS_TYPE"]['name']: 0.01, TYPE_MAP["NEG_TYPE"]['name']: 0.01},
                    a={TYPE_MAP["POS_TYPE"]['name']: 0.01, TYPE_MAP["NEG_TYPE"]['name']: 0.01})

            mc_sphere.shape_param.set("GFP_Pos", diameter=0)
            mc_sphere.shape_param.set("GFP_Neg", diameter=0)
            mc_sphere.shape_param.set("POS_CHARGE", diameter=0.05)
            mc_sphere.shape_param.set("NEG_CHARGE", diameter=0.05)
            log_quantities = ['time', 'hpmc_sweep', 'hpmc_overlap_count', 'hpmc_translate_acceptance', 'hpmc_rotate_acceptance', 'hpmc_d', 'hpmc_a', 'volume']

            # Dump shape info separately (POS seg faults when I try both)
            posfile_1 = fname_base + '1.pos'
            posfile_2 = fname_base + '2.pos'
            pos1 = deprecated.dump.pos(filename=posfile_1, period=1)
            mc_poly.setup_pos_writer(pos1)
            pos2 = deprecated.dump.pos(filename=posfile_2, period=2)
            mc_sphere.setup_pos_writer(pos2)

            hoomd.run(1)

            # Copy the existing files to a new one then modify
            posfile = fname_base + '.pos'

            with open(posfile_2) as f2:
                while True:
                    line = f2.readline()
                    if not line:
                        break
                    if "POS_CHARGE" not in line and "NEG_CHARGE" not in line:
                        continue
                    shape1 = line
                    shape2 = f2.readline()
                    break

            with open(posfile, 'w') as fw:
                with open(posfile_1) as f1:
                    # Write up until the bad shapedefs
                    while True:
                        line = f1.readline()
                        if "POS_CHARGE" not in line and "NEG_CHARGE" not in line:
                            fw.write(line)
                        else:
                            f1.readline()
                            break
                    # Write the correct sphere shapedefs
                    fw.write(shape1)
                    fw.write(shape2)
                    # Write the positions but remove quaternion for spheres
                    while True:
                        line = f1.readline()
                        if not line:
                            break
                        if "POS_CHARGE" not in line and "NEG_CHARGE" not in line:
                            fw.write(line)
                        else:
                            fw.write(line.replace("1 0 0 0", ''))

def test_charge(job, typ, actual = True):
    """This is a function to test whether two proteins
    with the same charge will cluster or not. It is a
    simple sanity check for the protomer simulations.
    It is effectively an operation, but in order to
    avoid code duplication it takes a typ argument so
    that the corresponding operations for positive and
    negative proteins can be defined trivially

    Args:
        job (signac.contrib.Job): The job to operate on
        typ (string): One of 'positive' or 'negative' (maybe more later)
        actual (bool): Whether to use real charges or fake them
    """
    # Initialize HOOMD if needed
    if hoomd.context.exec_conf is None:
        hoomd.context.initialize('--mode=cpu')
    with job:
        with hoomd.context.SimulationContext() as cont:
            # File name setup
            fname_base = typ + ('_actual' if actual else '_spoofed')
            log_file = fname_base + '.log'
            msd_file = fname_base + '_msd.log'
            xml_file = fname_base + '_0.xml'
            restart_file = fname_base + '_restart.gsd'
            output_file = fname_base + '.gsd'
            top_pos = job.fn("pos_top.itp")
            top_neg = job.fn("neg_top.itp")
            pos_file = fname_base + '.pos'
            seed = np.mod(np.prod(job.sp.negative_segments), 7**4) # Random construction

            # Initialization and setup
            system = hoomd.init.create_lattice(
                    hoomd.lattice.sc(a=5, type_name=TYPE_MAP["POS_TYPE"]['name']), n=1)
            system.replicate(nx=2)
            system.particles.types.add(TYPE_MAP["NEG_TYPE"]['name'])

            if typ in ['positive', 'negative']:
                type_to_use = "POS_TYPE" if typ == 'positive' else "NEG_TYPE"
                for p in system.particles:
                    p.type = TYPE_MAP[type_to_use]['name']
            elif typ == 'mixed':
                system.particles[0].type = TYPE_MAP["POS_TYPE"]['name']
                system.particles[1].type = TYPE_MAP["NEG_TYPE"]['name']
            else:
                raise RuntimeError("Unknown simulation type!")

            mc = hpmc.integrate.polyhedron(seed=int(seed), d = 0.1, a = 0.1)

            # Get face and vertex information
            DATA_POS = os.path.join(CONFIG_DIR, "triangulated_files", "2B3P_A_min_RK_-1.34_-1.01_conserv.pdb.data." + str(job.sp.repr_density))
            DATA_NEG = os.path.join(CONFIG_DIR, "triangulated_files", "2B3P_A_min_DE_-0.52_-0.66_conserv.pdb.data." + str(job.sp.repr_density))
            try:
                f1 = open(DATA_POS, 'rb')
                f2 = open(DATA_NEG, 'rb')
            except FileNotFoundError:
                print("No shape data files found for this density, regenerating now")
                pdb_to_shape.main(PDB_POS, density = job.sp.repr_density)
                pdb_to_shape.main(PDB_NEG, density = job.sp.repr_density)
                f1 = open(DATA_POS, 'rb')
                f2 = open(DATA_NEG, 'rb')
            finally:
                pos_data = pickle.load(f1)
                neg_data = pickle.load(f2)

            mc.shape_param.set(TYPE_MAP["POS_TYPE"]['name'],
                    vertices = pos_data['verts'],
                    faces = pos_data['faces'])
            mc.shape_param.set(TYPE_MAP["NEG_TYPE"]['name'],
                    vertices = neg_data['verts'],
                    faces = neg_data['faces'])
            mc.set_params(d={TYPE_MAP["POS_TYPE"]['name']: 0.01, TYPE_MAP["NEG_TYPE"]['name']: 0.01},
                    a={TYPE_MAP["POS_TYPE"]['name']: 0.01, TYPE_MAP["NEG_TYPE"]['name']: 0.01})

            log_quantities = ['time', 'hpmc_sweep', 'hpmc_overlap_count', 'hpmc_translate_acceptance', 'hpmc_rotate_acceptance', 'hpmc_d', 'hpmc_a', 'volume', 'hpmc_patch_energy']
            check_energy_computation = True
            if check_energy_computation:
                log_quantities.append('my_energy')

            pos_atoms = pos_data['atoms']
            neg_atoms = neg_data['atoms']
            charges_plus = get_charges(top_pos)
            charges_minus = get_charges(top_neg)

            if not actual:
                # Fake the charges for testing
                charges_plus = abs(charges_plus)
                charges_minus = -abs(charges_minus)

            # Defining functions to test that the yukawa energy being calculated is correct
            def compute_pair_energy(step, pos_a, pos_b, charge_a, charge_b, kappa, r_cut):
                # First compute pairwise distances, accounting for PBCs
                pos1 = np.copy(pos_a[:, np.newaxis, :]) # Copy shouldn't be necessary, but being safe
                pos2 = np.copy(pos_b[np.newaxis, :, :])
                pos1[:, :, 0] = np.mod(pos1[:, :, 0]+system.box.Lx/2, system.box.Lx)-system.box.Lx/2
                pos1[:, :, 1] = np.mod(pos1[:, :, 1]+system.box.Ly/2, system.box.Ly)-system.box.Ly/2
                pos1[:, :, 2] = np.mod(pos1[:, :, 2]+system.box.Lz/2, system.box.Lz)-system.box.Lz/2
                pos2[:, :, 0] = np.mod(pos2[:, :, 0]+system.box.Lx/2, system.box.Lx)-system.box.Lx/2
                pos2[:, :, 1] = np.mod(pos2[:, :, 1]+system.box.Ly/2, system.box.Ly)-system.box.Ly/2
                pos2[:, :, 2] = np.mod(pos2[:, :, 2]+system.box.Lz/2, system.box.Lz)-system.box.Lz/2

                vectors = pos1 - pos2
                # Minimum image convention
                vectors[:, :, 0] = np.where(np.abs(vectors[:, :, 0]) > system.box.Lx/2, system.box.Lx - np.abs(vectors[:, :, 0]), vectors[:, :, 0])
                vectors[:, :, 1] = np.where(np.abs(vectors[:, :, 1]) > system.box.Ly/2, system.box.Ly - np.abs(vectors[:, :, 1]), vectors[:, :, 1])
                vectors[:, :, 2] = np.where(np.abs(vectors[:, :, 2]) > system.box.Lz/2, system.box.Lz - np.abs(vectors[:, :, 2]), vectors[:, :, 2])

                distances = np.linalg.norm(vectors, axis = -1)
                energies = np.where(distances < r_cut,
                        0.7*charge_a[:, np.newaxis]*charge_b[np.newaxis, :]*np.exp(-kappa*distances)/distances,
                        0)
                return np.sum(energies)

            def compute_total_energy(step):
                tot_energy = 0
                for i, p1 in enumerate(system.particles):
                    for j in range(i+1, len(system.particles)):
                        p2 = system.particles[j]
                        if p1.type == TYPE_MAP['POS_TYPE']['name']:
                            pos1 = rowan.rotate(np.array(p1.orientation)[np.newaxis, :].repeat(len(pos_atoms), axis = 0), pos_atoms) + p1.position
                            charge1 = charges_plus
                        else:
                            pos1 = rowan.rotate(np.array(p1.orientation)[np.newaxis, :].repeat(len(neg_atoms), axis = 0), neg_atoms) + p1.position
                            charge1 = charges_minus
                        if p2.type == TYPE_MAP['POS_TYPE']['name']:
                            pos2 = rowan.rotate(np.array(p2.orientation)[np.newaxis, :].repeat(len(pos_atoms), axis = 0), pos_atoms) + p2.position
                            charge2 = charges_plus
                        else:
                            pos2 = rowan.rotate(np.array(p2.orientation)[np.newaxis, :].repeat(len(neg_atoms), axis = 0), neg_atoms) + p2.position
                            charge2 = charges_minus
                        # Make sure to rotate appropriately first
                        tot_energy += compute_pair_energy(step, pos1, pos2, charge1, charge2, job.sp.kappa, job.sp.r_cut_factor/job.sp.kappa)
                return tot_energy
                #print("The total energy at step {} is {}".format(step, tot_energy))

            # Dump files
            g_all = hoomd.group.all()
            gsd = hoomd.dump.gsd(
                filename=output_file, group=g_all, period=1, phase=0)
            restart = hoomd.dump.gsd(
                filename=restart_file, group=g_all, period=1, phase=0, truncate=True)
            log = hoomd.analyze.log(
                filename=log_file, header_prefix='#', quantities=log_quantities, period=1)
            if check_energy_computation:
                log.register_callback("my_energy", compute_total_energy)
            msd = deprecated.analyze.msd(filename=msd_file, groups=[g_all], period=1,
                    header_prefix='# ')
            pos = deprecated.dump.pos(filename=pos_file, period=10)
            mc.setup_pos_writer(pos)

            patch = jit.patch.user_union(mc,
                    r_cut=job.sp.r_cut_factor/job.sp.kappa,
                    code=gen_yukawa_potential(job.sp.kappa))
            patch.set_params(TYPE_MAP['POS_TYPE']['name'],
                    positions=pos_atoms,
                    typeids=[TYPE_MAP['POS_TYPE']['id']]*len(pos_atoms),
                    charges=charges_plus)
            patch.set_params(TYPE_MAP['NEG_TYPE']['name'],
                    positions=neg_atoms,
                    typeids=[TYPE_MAP['NEG_TYPE']['id']]*len(neg_atoms),
                    charges=charges_minus)

            # Add cluster moves
            if job.sp.use_clusters:
                seed_clusters = np.mod(np.prod(
                    job.sp.positive_segments), 7**4) # Random construction
                clusters = hpmc.update.clusters(mc,seed=seed_clusters)

            # Rescale box without scaling particles to allow cluster moves
            num_expansions = 0;
            hoomd.update.box_resize(L = 4*system.box.Lx, period = None, scale_particles = False)

            hoomd.run(1)
            while mc.count_overlaps() > 0:
                num_expansions += 1
                print('There are {} overlaps in your system. Will resize for the {} time'.format(
                    mc.count_overlaps(), num_expansions))
                if np.abs(system.box.Lx - system.box.Ly) > 1e-5 or np.abs(system.box.Lx != system.box.Lz) > 1e-5:
                    raise RuntimeError("Box is not cubic, box dimensions are ({}, {}, {}).".format(
                        system.box.Lx, system.box.Ly, system.box.Lz))
                hoomd.update.box_resize(L = 1.01*system.box.Lx, period = None)
                hoomd.run(1)

            hoomd.run(1000)

def test_all_positive(job):
    test_charge(job, 'positive', actual = False)

def test_all_negative(job):
    test_charge(job, 'negative', actual = False)

def test_all_mixed(job):
    test_charge(job, 'mixed', actual = False)

def test_positive(job):
    test_charge(job, 'positive')

def test_negative(job):
    test_charge(job, 'negative')

def test_mixed(job):
    test_charge(job, 'mixed')

if __name__ == '__main__':
    import flow
    flow.run()
