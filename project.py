from flow import FlowProject
#from flow import staticlabel
import flow
#from flow.environment import get_environment
import flow.environments  # uncomment to use default environments


#class Project(FlowProject):
#
#    @staticlabel()
#    def config_generated(job):
#        return job.isfile('init.gsd')
#
#    @staticlabel()
#    def evaluated(job):
#        return job.document.get('evaluated') is not None
#
#    @staticlabel()
#    def not_denatured(job):
#        return job.document.get('denatured') is None
#
#    @staticlabel()
#    def worth_simulation(job):
#        return job.document.get('quantile') is not None
#
#    @staticlabel()
#    def complete(job):
#        return job.document.get('finished') is not None
#
#    @staticlabel()
#    def invalid(job):
#        return False
#
#    @staticlabel()
#    def is_test(job):
#        return job.sp.get('test', None)
#
#    @staticlabel()
#    def is_not_test(job):
#        return job.sp.get('test', None) is None
#
#    @staticlabel()
#    def pos_finished(job):
#        return job.isfile('positive_actual.gsd')
#
#    @staticlabel()
#    def pos_all_finished(job):
#        return job.isfile('positive_spoofed.gsd')
#
#    @staticlabel()
#    def neg_finished(job):
#        return job.isfile('negative_actual.gsd')
#
#    @staticlabel()
#    def neg_all_finished(job):
#        return job.isfile('negative_spoofed.gsd')
#
#    @staticlabel()
#    def mixed_finished(job):
#        return job.isfile('mixed_actual.gsd')
#
#    @staticlabel()
#    def mixed_all_finished(job):
#        return job.isfile('mixed_spoofed.gsd')
#
#    def __init__(self, *args, **kwargs):
#        super(Project, self).__init__(*args, **kwargs)
#
#        self.add_operation(
#            name='gen_config',
#            cmd='python3 operations.py assemble_structure {job._id}',
#            pre=[Project.is_not_test],
#            post=[Project.config_generated],
#            np = 1,
#            )
#        self.add_operation(
#            name='evaluate',
#            #cmd='python3 operations.py evaluate_energy {job._id}',
#            cmd='OMP_NUM_THREADS=48 python3 operations.py evaluate_energy {job._id}',
#            pre=[Project.is_not_test, Project.config_generated, Project.not_denatured],
#            post=[Project.evaluated],
#            np = 1,
#            )
#        self.add_operation(
#            name='simulate',
#            cmd='python3 operations.py denature_protomer {job._id}',
#            pre=[Project.is_not_test, Project.evaluated, Project.worth_simulation],
#            post=[Project.complete],
#            np = 1,
#            )
#        self.add_operation(
#            name='test_all_mixed',
#            cmd='python3 operations.py test_all_mixed {job._id}',
#            pre=[Project.is_test],
#            post=[Project.mixed_all_finished],
#            np = 1,
#            )
#        self.add_operation(
#            name='test_all_positive',
#            cmd='python3 operations.py test_all_positive {job._id}',
#            pre=[Project.is_test],
#            post=[Project.pos_all_finished],
#            np = 1,
#            )
#        self.add_operation(
#            name='test_all_negative',
#            cmd='python3 operations.py test_all_negative {job._id}',
#            pre=[Project.is_test],
#            post=[Project.neg_all_finished],
#            np = 1,
#            )
#        self.add_operation(
#            name='test_mixed',
#            cmd='python3 operations.py test_mixed {job._id}',
#            pre=[Project.is_test],
#            post=[Project.mixed_finished],
#            np = 1,
#            )
#        self.add_operation(
#            name='test_positive',
#            cmd='python3 operations.py test_positive {job._id}',
#            pre=[Project.is_test],
#            post=[Project.pos_finished],
#            np = 1,
#            )
#        self.add_operation(
#            name='test_negative',
#            cmd='python3 operations.py test_negative {job._id}',
#            pre=[Project.is_test],
#            post=[Project.neg_finished],
#            np = 1,
#            )
#
#    def write_script_header(self, script, **kwargs):
#        """Override parent behavior to also add a HOOMD_WALLTIME_STOP"""
#        super(Project, self).write_script_header(script, **kwargs)
#        script.writeline("export HOOMD_WALLTIME_STOP=$((`date +%s` + {} - 2 * 60))".format(
#            int(kwargs['walltime'].total_seconds())
#            )
#            )
#        script.writeline("export OMP_NUM_THREADS=48")
#        script.writeline()
#
#    def write_script_operations(self, script, operations, background=False, **kwargs):
#        """Override parent behavior to enable automatic MPI."""
#        env = flow.get_environment()
#        for op in operations:
#            self.write_human_readable_statepoint(script, op.job)
#            if op.np > 1:
#                script.write_cmd(env.mpi_cmd(op.cmd.format(job=op.job), op.np), bg=background)
#            else:
#                script.write_cmd(op.cmd.format(job=op.job), bg=background)
#            script.writeline()

class Project(FlowProject):
    pass

@Project.label
def config_generated(job):
    return job.isfile('init.gsd')

@Project.label
def evaluated(job):
    return job.document.get('evaluated') is not None

@Project.label
def not_denatured(job):
    return job.document.get('denatured') is None

@Project.label
def worth_simulation(job):
    return job.document.get('quantile') is not None

@Project.label
def complete(job):
    return job.document.get('finished') is not None

@Project.label
def invalid(job):
    return False

@Project.label
def is_test(job):
    return job.sp.get('test', None)

@Project.label
def is_not_test(job):
    return job.sp.get('test', None) is None

@Project.label
def pos_finished(job):
    return job.isfile('positive_actual.gsd')

@Project.label
def pos_all_finished(job):
    return job.isfile('positive_spoofed.gsd')

@Project.label
def neg_finished(job):
    return job.isfile('negative_actual.gsd')

@Project.label
def neg_all_finished(job):
    return job.isfile('negative_spoofed.gsd')

@Project.label
def mixed_finished(job):
    return job.isfile('mixed_actual.gsd')

@Project.label
def mixed_all_finished(job):
    return job.isfile('mixed_spoofed.gsd')



@Project.operation
@flow.cmd
@Project.pre(is_not_test)
@Project.post(config_generated)
def gen_config(job):
    return 'python3 operations.py assemble_structure {job._id}'

@Project.operation
@flow.cmd
@Project.pre(is_not_test)
@Project.pre(config_generated)
@Project.pre(not_denatured)
@Project.post(evaluated)
@flow.directives(omp_num_threads=48, nranks=1)
def evaluate(job):
    return 'OMP_NUM_THREADS=48 python3 operations.py evaluate_energy {job._id}'

@Project.operation
@flow.cmd
@Project.pre(is_not_test)
@Project.pre(evaluated)
@Project.pre(worth_simulation)
@Project.post(complete)
def simulate(job):
    return 'python3 operations.py denature_protomer {job._id}'

@Project.operation
@flow.cmd
@Project.pre(is_test)
@Project.pre(mixed_all_finished)
def test_all_mixed(job):
    return 'python3 operations.py test_all_mixed {job._id}'

@Project.operation
@flow.cmd
@Project.pre(is_test)
@Project.pre(pos_all_finished)
def test_all_positive(job):
    return 'python3 operations.py test_all_positive {job._id}'

@Project.operation
@flow.cmd
@Project.pre(is_test)
@Project.pre(neg_all_finished)
def test_all_negative(job):
    return 'python3 operations.py test_all_negative {job._id}'

@Project.operation
@flow.cmd
@Project.pre(is_test)
@Project.pre(mixed_finished)
def test_mixed(job):
    return 'python3 test_erations.py test_mixed {job._id}'

@Project.operation
@flow.cmd
@Project.pre(is_test)
@Project.pre(pos_finished)
def test_positive(job):
    return 'python3 test_erations.py test_positive {job._id}'

@Project.operation
@flow.cmd
@Project.pre(is_test)
@Project.pre(neg_finished)
def test_negative(job):
    return 'python3 operations.py test_negative {job._id}'


if __name__ == '__main__':
    Project().main()
