import argparse
import fileinput

def main(gsdfile):
    # First get colors
    posfile = gsdfile.replace('.gsd', '')
    pos_color = None
    neg_color = None
    with open(posfile) as pf:
        for line in pf:
            if 'def GFP_Pos' in line:
                pos_color = line[-10:]
            elif 'def GFP_Neg' in line:
                neg_color = line[-10:]
            if pos_color is not None and neg_color is not None:
                break

    for line in fileinput.input(files=[gsdfile], inplace=True, backup='.bak'):
        if 'def GFP_Pos' in line:
            line = line[:-10] + pos_color
            print(line, end = '')
        elif 'def GFP_Neg' in line:
            line = line[:-10] + neg_color
            print(line, end = '')
        else:
            print(line, end = '')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Add colors from .pos to .gsd.pos file in a signac workspace dir"
            )
    parser.add_argument(
            "gsdfile",
            type=str,
            help="The gsd file to modify"
            )
    args = parser.parse_args()
    main(**vars(args))
