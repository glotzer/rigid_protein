import signac
import os
import glob
import subprocess

project = signac.get_project()
other_project = signac.get_project("../candidate_stability/")

pdb_file = 'proposed_protomer.pdb'

def get_charge(top_file):
    """Parse a topology file to find the total charge"""
    search = subprocess.check_output(['grep', 'qtot', top_file]).decode('ascii').strip().split('\n')
    last = search[-1]
    return float(last[last.find('qtot'):].replace('qtot', '').strip())

for job in project:
    if not job.isfile(pdb_file):
        query_dict = dict(
                pos_type = job.sp.pos_type,
                positive_segments = job.sp.positive_segments,
                negative_segments = job.sp.negative_segments
                )
        if job.sp.pos_type == "GFP":
            query_dict['rotation'] = 0
            query_dict['salt_concentration'] = 0.2
        other_job = next(other_project.find_jobs(query_dict))
        with job:
            # Link the initial file into the new job. Use relative
            # path so it works when transferring to comet
            os.symlink(os.path.relpath(other_job.fn(pdb_file)), job.fn(pdb_file))

            # Find a positive and a negative topology to use for this
            pos_file = None
            neg_file = None
            with other_job:
                for fn in glob.glob('topol*.itp'):
                    # Search file for the charge
                    c = get_charge(fn)
                    if c > 0 and pos_file is None:
                        pos_file = fn
                    elif c < 0 and neg_file is None:
                        neg_file = fn
                    if pos_file is not None and neg_file is not None:
                        break
            os.symlink(os.path.relpath(other_job.fn(pos_file)), job.fn('pos_top.itp') )
            os.symlink(os.path.relpath(other_job.fn(neg_file)), job.fn('neg_top.itp'))
