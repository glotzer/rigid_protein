#!/usr/bin/python
import os
import signac
import shutil
import datetime
import argparse

def main(args):
    project = signac.get_project()

    with project.open_job(id = args.job) as j:
        timestr = datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S")
        bkp_folder = 'backup_' + timestr
        for fn in os.listdir('.'):
            if fn not in ['signac_statepoint.json', 'pos_top.itp', 'neg_top.itp', 'proposed_protomer.pdb']:
                # Lazy way to ensure it only makes a folder when there are files to move
                if not os.path.exists(bkp_folder):
                    os.makedirs(bkp_folder)
                shutil.move(fn, bkp_folder)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Move existing workspace into backup folder for testing")
    parser.add_argument(
            "job",
            type=str,
            help="The job to backup")
    parser.add_argument(
            "-t", "--tag",
            type=str,
            default=None,
            help="Optional tag to add to the backed up workspace name")
    args = parser.parse_args()
    main(args)
