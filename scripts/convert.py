from glotzformats.reader import *
from glotzformats.writer import *

pos_reader = PosFileReader()
pos_writer = PosFileWriter()

gsd_reader = GSDHOOMDFileReader()

import sys
with open(sys.argv[1]) as posfile:
    with open(sys.argv[2], 'rb') as gsdfile:
        pos_frame = pos_reader.read(posfile)[0]
        traj = gsd_reader.read(gsdfile, pos_frame)
        num_frames = len(traj)
        if len(sys.argv) > 3:
            num_frames = int(sys.argv[3])
        step = 1
        if len(sys.argv) > 4:
            step = int(sys.argv[4])

        print('Converting {} frames'.format(num_frames))
        with open(sys.argv[2]+'.pos', 'w', encoding='utf-8') as out:
            pos_writer.write(traj[-num_frames::step], out)
