import signac
import subprocess
project = signac.get_project()

cmd = ["python", project.fn("convert.py"), 'evaluate.pos', 'evaluate.gsd', '1']
for job in project:
    with job:
        subprocess.call(cmd)
