pos_chains='C,D,G,H,K,L,O,P'
neg_chains='A,B,E,F,I,J,M,N'
pos_rep_str = lambda resnum: ':{}.'.format(resnum) + (str(resnum)+ '.').join(x + ',' for x in pos_chains.split(','))[:-1]
neg_rep_str = lambda resnum: ':{}.'.format(resnum) + (str(resnum)+ '.').join(x + ',' for x in neg_chains.split(','))[:-1]
