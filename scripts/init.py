#!/usr/bin/env python
"""Initialize the project's data space.
This init script loops over the parameter space of interest and copies
jobs from the candidate_structures project for the initial structures
"""
from __future__ import print_function, division, absolute_import

import argparse
import signac
import shutil
import subprocess
import os
import json
import glob
import numpy as np

def get_charge(top_file):
    """Parse a topology file to find the total charge"""
    search = subprocess.check_output(['grep', 'qtot', top_file]).decode('ascii').strip().split('\n')
    last = search[-1]
    return float(last[last.find('qtot'):].replace('qtot', '').strip())

def main_refined(args):
    """Perform a finer sweep"""
    # Switch to main workspace

    # Basing this on the files in the candidate structures project
    other_project = signac.get_project("../candidate_stability/")
    project = signac.get_project()

    statepoints_init = []

    config_folder = project.fn("ConfigFiles")
    pdb_file = 'proposed_protomer.pdb'

    densities = [1.0]
    r_cut_factors = [3.0]
    cluster_usage = [True]
    num_replicas = 5

    # Start calculating
    min_kappa = 0.74
    max_kappa = 1.2
    delta_kappa = 0.02
    kappas = np.linspace(min_kappa, max_kappa, round((max_kappa-min_kappa)/delta_kappa) + 1)
    pos_types = ["refined"]

    # This is for the broader sweep, which includes more kappa values than needed
    for pos_type in pos_types:
        other_job = next(other_project.find_jobs(dict(pos_type=pos_type)))
        for n in range(1, num_replicas+1):
            for kappa in kappas:
                for density in densities:
                    for r_cut_factor in r_cut_factors:
                        for use_clusters in cluster_usage:

                            # Remove unnecessary elements of the statepoint before creating the job
                            sp = other_job.statepoint()
                            sp['kappa'] = kappa
                            sp['repr_density'] = density
                            sp['use_clusters'] = use_clusters
                            sp['r_cut_factor'] = r_cut_factor
                            sp['run_num'] = n
                            sp['pos_type'] = pos_type
                            sp['shift'] = False

                            job = project.open_job(sp)
                            if job in project:
                                continue
                            job.init()

                            # Enter job context so that symlinks are correct
                            with job:
                                # Link the initial file into the new job. Use relative
                                # path so it works when transferring to comet
                                os.symlink(os.path.relpath(other_job.fn(pdb_file)), job.fn(pdb_file))

                                # Find a positive and a negative topology to use for this
                                pos_file = None
                                neg_file = None
                                with other_job:
                                    for fn in glob.glob('topol*.itp'):
                                        # Search file for the charge
                                        c = get_charge(fn)
                                        if c > 0 and pos_file is None:
                                            pos_file = fn
                                        elif c < 0 and neg_file is None:
                                            neg_file = fn
                                        if pos_file is not None and neg_file is not None:
                                            break
                                os.symlink(os.path.relpath(other_job.fn(pos_file)), job.fn('pos_top.itp') )
                                os.symlink(os.path.relpath(other_job.fn(neg_file)), job.fn('neg_top.itp'))
    return

def main_low_kappa(args):
    """Perform a finer sweep"""
    # Switch to main workspace
    #config = signac.common.config.get_config('signac.rc')
    #config.update(dict(workspace_dir='workspace'))
    #config.write()
    #project = signac.get_project()

    # Basing this on the files in the candidate structures project
    other_project = signac.get_project("../candidate_stability/")
    try:
        project = signac.get_project()
    except LookupError:
        project = signac.init_project('RigidProtein')

    statepoints_init = []

    config_folder = project.fn("ConfigFiles")
    pdb_file = 'proposed_protomer.pdb'

    densities = [1.0]
    r_cut_factors = [3.0]
    cluster_usage = [True]
    num_replicas = 5

    # Start calculating
    #min_kappa = 0.5 # Override old values
    #max_kappa = 0.8
    #delta_kappa = 0.02
    min_kappa = 0.56
    max_kappa = 0.74
    delta_kappa = 0.02
    kappas = np.linspace(min_kappa, max_kappa, round((max_kappa-min_kappa)/delta_kappa) + 1)

    # Skipping GFP runs going forward
    #pos_types = ["GFP", "Ceru"]
    pos_types = ["Ceru"]

    # This is for the broader sweep, which includes more kappa values than needed
    for n in range(1, num_replicas+1):
        for kappa in kappas:
            for density in densities:
                for r_cut_factor in r_cut_factors:
                    for use_clusters in cluster_usage:
                        for pos_type in pos_types:
                            query_dict = dict(pos_type=pos_type)

                            if pos_type == "GFP":
                                # rotation and salt irrelevant for these sims, just need to avoid duplicates
                                # Not a problem for Ceru since i never added those keys
                                query_dict['rotation'] = 0
                                query_dict['salt_concentration'] = 0.2
                            jobs = other_project.find_jobs(query_dict)
                            # This will be a loop over the different configurations
                            for other_job in jobs:
                                # Remove unnecessary elements of the statepoint before creating the job
                                sp = other_job.statepoint()
                                if sp.get('mutation') is not None:
                                    # Not bothering with any mutations any more
                                    continue
                                if pos_type == "GFP":
                                    del sp['salt_concentration']
                                sp['kappa'] = kappa
                                sp['repr_density'] = density
                                sp['use_clusters'] = use_clusters
                                sp['r_cut_factor'] = r_cut_factor
                                sp['run_num'] = n
                                sp['pos_type'] = pos_type

                                job = project.open_job(sp)
                                if job in project:
                                    continue
                                job.init()

                                # Enter job context so that symlinks are correct
                                with job:
                                    # Link the initial file into the new job. Use relative
                                    # path so it works when transferring to comet
                                    os.symlink(os.path.relpath(other_job.fn(pdb_file)), job.fn(pdb_file))

                                    # Find a positive and a negative topology to use for this
                                    pos_file = None
                                    neg_file = None
                                    with other_job:
                                        for fn in glob.glob('topol*.itp'):
                                            # Search file for the charge
                                            c = get_charge(fn)
                                            if c > 0 and pos_file is None:
                                                pos_file = fn
                                            elif c < 0 and neg_file is None:
                                                neg_file = fn
                                            if pos_file is not None and neg_file is not None:
                                                break
                                    os.symlink(os.path.relpath(other_job.fn(pos_file)), job.fn('pos_top.itp') )
                                    os.symlink(os.path.relpath(other_job.fn(neg_file)), job.fn('neg_top.itp'))
    return

def main_low_rotation(args):
    """Sweep for using minimal rotation"""
    # Switch to main workspace
    #config = signac.common.config.get_config('signac.rc')
    #config.update(dict(workspace_dir='workspace'))
    #config.write()
    #project = signac.get_project()

    # Basing this on the files in the candidate structures project
    other_project = signac.get_project("../candidate_stability/")
    try:
        project = signac.get_project()
    except LookupError:
        project = signac.init_project('RigidProtein')

    statepoints_init = []

    config_folder = project.fn("ConfigFiles")
    pdb_file = 'proposed_protomer.pdb'

    densities = [1.0]
    r_cut_factors = [3.0]
    cluster_usage = [True]
    num_replicas = 5

    # Start calculating
    #min_kappa = 0.5 # Override old values
    #max_kappa = 0.8
    #delta_kappa = 0.02
    min_kappa = 0.6
    max_kappa = 0.74
    delta_kappa = 0.02
    kappas = np.linspace(min_kappa, max_kappa, round((max_kappa-min_kappa)/delta_kappa) + 1)

    # Skipping GFP runs going forward
    #pos_types = ["GFP", "Ceru"]
    pos_types = ["Ceru"]

    # This is for the broader sweep, which includes more kappa values than needed
    for n in range(1, num_replicas+1):
        for kappa in kappas:
            for density in densities:
                for r_cut_factor in r_cut_factors:
                    for use_clusters in cluster_usage:
                        for pos_type in pos_types:
                            query_dict = dict(pos_type=pos_type)

                            if pos_type == "GFP":
                                # rotation and salt irrelevant for these sims, just need to avoid duplicates
                                # Not a problem for Ceru since i never added those keys
                                query_dict['rotation'] = 0
                                query_dict['salt_concentration'] = 0.2
                            jobs = other_project.find_jobs(query_dict)
                            # This will be a loop over the different configurations
                            for other_job in jobs:
                                # Remove unnecessary elements of the statepoint before creating the job
                                sp = other_job.statepoint()
                                if sp.get('mutation') is not None:
                                    # Not bothering with any mutations any more
                                    continue
                                if pos_type == "GFP":
                                    del sp['salt_concentration']
                                sp['kappa'] = kappa
                                sp['repr_density'] = density
                                sp['use_clusters'] = use_clusters
                                sp['r_cut_factor'] = r_cut_factor
                                sp['run_num'] = n
                                sp['pos_type'] = pos_type
                                sp['low_rotation'] = True

                                job = project.open_job(sp)
                                if job in project:
                                    continue
                                job.init()

                                # Enter job context so that symlinks are correct
                                with job:
                                    # Link the initial file into the new job. Use relative
                                    # path so it works when transferring to comet
                                    os.symlink(os.path.relpath(other_job.fn(pdb_file)), job.fn(pdb_file))

                                    # Find a positive and a negative topology to use for this
                                    pos_file = None
                                    neg_file = None
                                    with other_job:
                                        for fn in glob.glob('topol*.itp'):
                                            # Search file for the charge
                                            c = get_charge(fn)
                                            if c > 0 and pos_file is None:
                                                pos_file = fn
                                            elif c < 0 and neg_file is None:
                                                neg_file = fn
                                            if pos_file is not None and neg_file is not None:
                                                break
                                    os.symlink(os.path.relpath(other_job.fn(pos_file)), job.fn('pos_top.itp') )
                                    os.symlink(os.path.relpath(other_job.fn(neg_file)), job.fn('neg_top.itp'))
    return

def test_main(args):
    other_project = signac.get_project("../candidate_stability/")
    try:
        project = signac.get_project()
    except LookupError:
        project = signac.init_project('RigidProtein')

    # Switch to test workspace
    #config = signac.common.config.get_config('signac.rc')
    #config.update(dict(workspace_dir='workspace_test'))
    #config.write()
    #project = signac.get_project()

    config_folder = project.fn("ConfigFiles")
    pdb_file = 'proposed_protomer.pdb'


    jobs = other_project.find_jobs(
            dict(
                rotation=0,
                salt_concentration=0.2
                ))

    # Randomly choose one for now
    other_job = next(jobs)

    #for kappa in [0.5, 1]:
    for kappa in [1]:
        for use_clusters in [True, False]:
            for r_cut_factor in [8.0]:
                sp = other_job.statepoint().copy()
                del sp['salt_concentration'], sp['rotation']
                sp['kappa'] = kappa
                sp['r_cut_factor'] = r_cut_factor
                sp['repr_density'] = 1.0
                sp['use_clusters'] = use_clusters
                sp['test'] = True
                job = project.open_job(sp)
                job.init()

                # Copy the initial file into the new job
                os.symlink(other_job.fn(pdb_file), job.fn(pdb_file))

                # Find a positive and a negative topology to use for this
                pos_file = None
                neg_file = None
                with other_job:
                    for fn in glob.glob('topol*.itp'):
                        # Search file for the charge
                        c = get_charge(fn)
                        if c > 0 and pos_file is None:
                            pos_file = fn
                        elif c < 0 and neg_file is None:
                            neg_file = fn
                        if pos_file is not None and neg_file is not None:
                            break
                os.symlink(other_job.fn(pos_file), job.fn('pos_top.itp'))
                os.symlink(other_job.fn(neg_file), job.fn('neg_top.itp'))

def test_depletion(args):
    other_project = signac.get_project("../candidate_stability/")
    try:
        project = signac.get_project()
    except LookupError:
        project = signac.init_project('RigidProtein')

    # Switch to test workspace
    config = signac.common.config.get_config('signac.rc')
    config.update(dict(workspace_dir='test_depletion_workspace'))
    config.write()
    project = signac.get_project()

    config_folder = project.fn("ConfigFiles")
    pdb_file = 'proposed_protomer.pdb'


    jobs = other_project.find_jobs(
            dict(
                rotation=0,
                salt_concentration=0.2
                ))

    # Randomly choose one for now
    other_job = next(jobs)

    for kappa in [1]:
        for use_clusters in [True, False]:
            for r_cut_factor in [8.0]:
                sp = other_job.statepoint().copy()
                del sp['salt_concentration'], sp['rotation']
                sp['kappa'] = kappa
                sp['r_cut_factor'] = r_cut_factor
                sp['repr_density'] = 1.0
                sp['use_clusters'] = use_clusters
                sp['dep'] = True
                job = project.open_job(sp)
                job.init()

                # Copy the initial file into the new job
                os.symlink(other_job.fn(pdb_file), job.fn(pdb_file))

                # Find a positive and a negative topology to use for this
                pos_file = None
                neg_file = None
                with other_job:
                    for fn in glob.glob('topol*.itp'):
                        # Search file for the charge
                        c = get_charge(fn)
                        if c > 0 and pos_file is None:
                            pos_file = fn
                        elif c < 0 and neg_file is None:
                            neg_file = fn
                        if pos_file is not None and neg_file is not None:
                            break
                os.symlink(other_job.fn(pos_file), job.fn('pos_top.itp'))
                os.symlink(other_job.fn(neg_file), job.fn('neg_top.itp'))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Initialize the data space.")
    parser.add_argument(
            "-t", "--test",
            action="store_true",
            help="Whether to construct a test workspace or not")
    parser.add_argument(
            "-d", "--test_depletion",
            action="store_true",
            help="Construct a workspace for testing depletion")

    args = parser.parse_args()

    if args.test:
        test_main(args)
    elif args.test_depletion:
        test_depletion(args)
    else:
        #main_low_kappa(args)
        main_refined(args)
