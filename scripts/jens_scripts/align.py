import numpy as np
import math

import sys

def conj(q):
    return np.array((q[0],-q[1],-q[2],-q[3]))

def rotate(q,v):
    q = np.array(q)
    v = np.array(v)
    return np.array((q[0]*q[0]-np.dot(q[1:],q[1:]))*v+2*q[0]*np.cross(q[1:],v)+2*np.dot(q[1:],v)*q[1:])

def quat_mult(a,b):
    a = np.array(a)
    b = np.array(b)
    s = a[0]*b[0] - np.dot(a[1:],b[1:])
    v = a[0]*b[1:] + b[0]*a[1:] + np.cross(a[1:],b[1:])
    return np.array((s,v[0],v[1],v[2]))

def quaternion_from_matrix(T):
    # http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
    tr = np.trace(T)
    if (tr > 0):
        S = math.sqrt(tr+1.0) * 2;
        qw = 0.25 * S;
        qx = (T[2,1] - T[1,2])/S;
        qy = (T[0,2] - T[2,0])/S;
        qz = (T[1,0] - T[0,1])/S;
    elif ((T[0,0] > T[1,1]) and (T[0,0] > T[2,2])):
        S = math.sqrt(1.0+T[0,0]-T[1,1]-T[2,2])*2;
        qw = (T[2,1] - T[1,2]) / S;
        qx = 0.25*S;
        qy = (T[0,1] + T[1,0]) / S;
        qz = (T[0,2] + T[2,0]) / S;
    elif (T[1,1] > T[2,2]):
        S = math.sqrt(1.0 + T[1,1] - T[0,0] - T[2,2])*2
        qw = (T[0,2] - T[2,0]) / S;
        qx = (T[0,1] + T[1,0]) / S;
        qy = 0.25*S;
        qz = (T[1,2] + T[2,1]) /S;
    else:
        S = math.sqrt(1.0 + T[2,2] - T[0,0] - T[1,1])*2;
        qw = (T[1,0] - T[0,1]) / S;
        qx = (T[0,2] + T[2,0]) / S;
        qy = (T[1,2] + T[2,1]) / S;
        qz = 0.25 * S;
    return np.array((qw,qx,qy,qz))

from pdb import *
pdb_assembly = pdb()
pdb_assembly.read(sys.argv[1])

pdb_plus = pdb()
pdb_plus.read(sys.argv[2])

pdb_minus = pdb()
pdb_minus.read(sys.argv[3])

print('+: {} atoms'.format(len(pdb_plus.chain_atoms[0])))
atoms_plus = np.array(np.array(pdb_plus.pos)/10)
centroid_plus = np.mean(atoms_plus, axis=0)
atoms_plus -= centroid_plus

print('-: {} atoms'.format(len(pdb_minus.chain_atoms[0])))
atoms_minus = np.array(np.array(pdb_minus.pos)/10)
centroid_minus = np.mean(atoms_minus, axis=0)
atoms_minus -= centroid_minus

import gsd.fl
import gsd.hoomd

snap = gsd.hoomd.Snapshot()

snap.particles.N = len(pdb_assembly.chain_ids)
print(snap.particles.N)

pos = []
orientation = []

offset = 0
types = []
for i,(chain_id, chain_atoms) in enumerate(zip(pdb_assembly.chain_ids,pdb_assembly.chain_atoms)):
    print('Chain {} {} atoms'.format(chain_id,len(chain_atoms)))

    atoms = np.array(np.array(pdb_assembly.pos[offset:offset+len(chain_atoms)])/10)
    offset += len(chain_atoms)
    centroid_B = np.mean(atoms,axis=0)

    is_plus = len(atoms) == len(atoms_plus)

    if is_plus:
        types.append(0)
        print('+')
    else:
        types.append(1)
        print('-')

    centroid_A = np.zeros(3)
    if is_plus:
        AA = atoms_plus - np.tile(centroid_A, (len(atoms_plus),1))
    else:
        AA = atoms_minus - np.tile(centroid_A, (len(atoms_minus),1))

    BB = atoms - np.tile(centroid_B, (len(atoms), 1))

    H = np.dot(np.transpose(AA),BB)
    U, S, Vt = np.linalg.svd(H)
    R = np.dot(Vt.T,U.T)

    # special reflection case
    if np.linalg.det(R) < 0:
       print('Reflection detected')
       Vt[2,:] *= -1
       R = np.dot(Vt.T, U.T)

    t = np.dot(-R,centroid_A.T) + centroid_B.T
    pos.append(t)
    orientation.append(quaternion_from_matrix(R))

pos = np.array(pos)
pos -= np.mean(pos,axis=0)
snap.particles.position = pos
print(snap.particles.position)
snap.particles.orientation = np.array(orientation)
snap.particles.typeid = np.array(types)
snap.particles.types = ['ceru+36','gfp-18']

r_max = np.max(np.linalg.norm(pos,axis=1)) + np.max(np.linalg.norm(atoms_plus,axis=1))
L = 2*r_max
snap.configuration.box = np.array([L,L,L,0,0,0], dtype=np.float32)

gsd.hoomd.create(sys.argv[4], snap)
