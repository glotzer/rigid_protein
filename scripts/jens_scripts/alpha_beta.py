from hoomd import *
from hoomd import hpmc

import signac

import math

import numpy as np

from helpers import *

context.initialize()

def eigen_sphere(verts):
    cov= np.cov(np.asarray(verts).transpose())
    w,v = np.linalg.eig(cov)
    idx = np.argsort(w)[::-1]
    extent = np.dot(v[idx[0]]/np.linalg.norm(v[idx[0]]),verts.transpose())
    min_pt = np.argmin(extent)
    max_pt = np.argmax(extent)
    return 0.5*(verts[max_pt]+verts[min_pt]),0.5*(extent[max_pt]-extent[min_pt])

def sphere_of_sphere_and_pt(c,r,p):
    d = p - c;
    dist2 = np.dot(d,d);
    if dist2 > r*r:
        dist = math.sqrt(dist2);
        new_r = (r + dist) * 0.5;
        k = (new_r - r) /dist
        return c+d*k,new_r
    else:
        return c,r

def ritter_eigen_sphere(verts):
    c,r = eigen_sphere(verts)
    for v in verts:
        c,r = sphere_of_sphere_and_pt(c,r,v)
    return c,r

project = signac.get_project()

sp  = {'phi_p': 0.3, 'N': 1, 'density': 1.0, 'edges': 0, 'V_alpha': 16.58, 'V_beta': 17.49, 'hcut': 0, 'initialize_close': False, 'seed': 12345}
#wr_par = {'sigma': 0.21, 'mu_A': 0.349, 'delta_mu': -0.01}
wr_par = {'sigma': 0.141, 'mu_A': 0.349, 'delta_mu': -0.05}
sp.update(wr_par)

import os
input_alpha = '2hhb_alpha_{:.1f}_0.0.xml'.format(sp['density'])
input_beta = '2hhb_beta_{:.1f}_0.0.xml'.format(sp['density'])

vertices_alpha,faces_alpha,colors_alpha = read_vtk(input_alpha)
vertices_beta,faces_beta,colors_beta = read_vtk(input_beta)

vertices_alpha =np.array(vertices_alpha)
vertices_beta =np.array(vertices_beta)

with project.open_job(sp) as job:
    base = job.workspace()+'/trajectory'
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print('after open_job {}'.format(dir_path))

#    root = signac.get_project().root_directory()
    # center

    rcm_alpha,circ_r_alpha = ritter_eigen_sphere(vertices_alpha)
    vertices_alpha = vertices_alpha - rcm_alpha

    # CGAL
    origin_alpha = np.array((1.05573,-0.12935,-1.64397)) - rcm_alpha

    rcm_beta,circ_r_beta = ritter_eigen_sphere(vertices_beta)

    print(circ_r_alpha, circ_r_beta)
    vertices_beta = vertices_beta - rcm_beta
    origin_beta = np.array((1.62937,-1.09577,-0.261639)) - rcm_beta

    from hoomd import deprecated
    phi_p_ini = 0.001
    N = 2*sp['N']
    L_ini = (sp['N']*(sp['V_alpha']+sp['V_beta'])/phi_p_ini)**(1./3.)
    L_target = (sp['N']*(sp['V_alpha']+sp['V_beta'])/sp['phi_p'])**(1./3.)

    #system = deprecated.init.create_random(N=N,phi_p=phi_p_ini, min_dist=2.0*circ_r,dimensions=2)
    system = deprecated.init.create_random(N=N,box=data.boxdim(L=L_ini), min_dist=2.0*max(circ_r_alpha,circ_r_beta),seed=123)
    #system = init.read_gsd('1ema_uc_mc_N16_gpu_ballcover_n1gfl_16_3_PV6.000.gsd',frame=14)
    #system = deprecated.init.read_xml('1ema_uc.xml')
    #system = deprecated.init.read_xml('1gfl_uc.xml')

    system.particles.types.add('B')
    for p in system.particles:
        if p.tag % 2 == 0:
            p.type = 'A'
        else:
            p.type = 'B'

    if sp['initialize_close']:
        for p in system.particles:
            if p.tag % 2 == 0:
                q = np.array(p.orientation)
                pos = p.position + rotate(q,rcm_alpha)
                p.position = pos
            else:
                q = np.array(p.orientation)
                pos = system.particles.get(p.tag-1).position - rotate(q,rcm_alpha) + rotate(q,rcm_beta)
                p.position = pos + rotate(q,rcm_beta-rcm_alpha)*0.04

    mc = hpmc.integrate.polyhedron(seed=1234)

    overlap_alpha = []
    for f in faces_alpha:
        h = np.mean([colors_alpha[i] for i in f])

        if h >= sp['hcut']:
            overlap_alpha.append(2)
        else:
            overlap_alpha.append(3)

    overlap_beta = []
    for f in faces_beta:
        h = np.mean([colors_beta[i] for i in f])

        if h >= sp['hcut']:
            overlap_beta.append(2)
        else:
            overlap_beta.append(3)

    mc.shape_param.set('A',vertices=vertices_alpha, faces=faces_alpha, origin=origin_alpha,overlap=overlap_alpha)
    mc.shape_param.set('B',vertices=vertices_beta, faces=faces_beta,origin=origin_beta,overlap=overlap_beta)

    system.particles.types.add('C')
    system.particles.types.add('D')

    mc.shape_param.set('C', vertices=[(0,0,0)], faces=[[0]], sweep_radius=0.5*sp['sigma'],overlap=[3]) # liquid
    mc.shape_param.set('D', vertices=[(0,0,0)], faces=[[0]], sweep_radius=0.5*sp['sigma'],overlap=[1],ignore_statistics=True) # vapor
    mc.overlap_checks.set('C','C',False)
    mc.overlap_checks.set('D','D',False)

    fugacity = math.exp(sp['mu_A'])/sp['sigma']**3.0

    mc.set_params(d=0.15,a=0.1)
#    mc.set_params(d=0,a=0)
    mc_tune_A = hpmc.util.tune(mc, tunables=['d','a'],max_val=[4,0.5],gamma=1,target=0.3,type='A')
    mc_tune_B = hpmc.util.tune(mc, tunables=['d','a'],max_val=[4,0.5],gamma=1,target=0.3,type='B')

    log = analyze.log(filename=base+'.log', quantities = ['hpmc_overlap_count','hpmc_muvt_N_C','hpmc_muvt_N_D','volume', 'hpmc_d_A','hpmc_a_A','hpmc_d_B','hpmc_a_B','time'], overwrite=True, period=100)

    run(1)
    # shrink the box to the target (if needed)
    scale = 0.99
    L = L_ini;
    while L_target < L:
        # shrink the box
        L = max(L*scale, L_target);

        update.box_resize(Lx=L, Ly=L, Lz=L, period=None);
        overlaps = mc.count_overlaps();
        context.msg.notice(1,"phi =%f: overlaps = %d " % (((sp['N']*(sp['V_alpha']+sp['V_beta'])) / (L*L*L)), overlaps));

        # run until all overlaps are removed
        while overlaps > 0:
            #mc_tune.update()
            run(100, quiet=True);
            overlaps = mc.count_overlaps();
            context.msg.notice(1,"%d\n" % overlaps)

        context.msg.notice(1,"\n");

    update.box_resize(Lx=L_target, Ly=L_target, Lz=L_target, period=None);

    if comm.get_rank() == 0:
        meta.dump_metadata(filename=base+'_meta.json')

    muvt = hpmc.update.muvt(mc=mc,period=1,seed=sp['seed'],transfer_types=['C'])
    #fugacity_D = fugacity*math.exp(-fugacity*math.pi*4.0/3.0*sp['sigma']**3.0)

    muvt.set_fugacity('C',fugacity)

    pos = deprecated.dump.pos(filename=base+'.pos',period=1)
    gsd = dump.gsd(base+'.gsd', period=100, overwrite=True,group=group.all(),dynamic=['attribute','property'])

    mc.setup_pos_writer(pos,colors=dict(A='ff5984ff',B='ffff0000'))
    pos.set_def('C','sphere {} ff0000ff'.format(sp['sigma']))
    pos.set_def('D','sphere {} ffffffff'.format(sp['sigma']))
    run(1)
    pos.disable()

    clusters = hpmc.update.clusters(mc,seed=123)
    clusters.set_params(swap_types=('C','D'))
    #clusters.set_params(swap_move_ratio=0)
    clusters.set_params(delta_mu=sp['delta_mu'])
    #clusters.set_params(move_ratio=0)

    mc.set_params(d=dict(C=0,D=0),a=dict(C=0,D=0))

    #pos.disable()
    for i in range(100):
        run(10,profile=True)
        mc_tune_A.update()
        mc_tune_B.update()

    run(1e8)
