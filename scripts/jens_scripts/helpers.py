import math
import numpy as np

def quaternion_from_matrix(T):
    tr = np.trace(T)
    if (tr > 0):
        S = math.sqrt(tr+1.0) * 2;
        qw = 0.25 * S;
        qx = (T[2,1] - T[1,2])/S;
        qy = (T[0,2] - T[2,0])/S;
        qz = (T[1,0] - T[0,1])/S;
    elif ((T[0,0] > T[1,1]) and (T[0,0] > T[2,2])):
        S = math.sqrt(1.0+T[0,0]-T[1,1]-T[2,2])*2;
        qw = (T[2,1] - T[1,2]) / S;
        qx = 0.25*S;
        qy = (T[0,1] + T[1,0]) / S;
        qz = (T[0,2] + T[2,0]) / S;
    elif (T[1,1] > T[2,2]):
        S = math.sqrt(1.0 + T[1,1] - T[0,0] - T[2,2])*2
        qw = (T[0,2] - T[2,0]) / S;
        qx = (T[0,1] + T[1,0]) / S;
        qy = 0.25*S;
        qz = (T[1,2] + T[2,1]) /S;
    else:
        S = math.sqrt(1.0 + T[2,2] - T[0,0] - T[1,1])*2;
        qw = (T[1,0] - T[0,1]) / S;
        qx = (T[0,2] + T[2,0]) / S;
        qy = (T[1,2] + T[2,1]) / S;
        qz = 0.25 * S;
    return np.array((qw,qx,qy,qz))

def conj(q):
    return np.array((q[0],-q[1],-q[2],-q[3]))

def rotate(q,v):
    q = np.array(q)
    v = np.array(v)
    return np.array((q[0]*q[0]-np.dot(q[1:],q[1:]))*v+2*q[0]*np.cross(q[1:],v)+2*np.dot(q[1:],v)*q[1:])

def quat_mult(a,b):
    a = np.array(a)
    b = np.array(b)
    s = a[0]*b[0] - np.dot(a[1:],b[1:])
    v = a[0]*b[1:] + b[0]*a[1:] + np.cross(a[1:],b[1:])
    return np.array((s,v[0],v[1],v[2]))

def read_off(file):
    header = file.readline().strip()
    if header != 'OFF':
        throw('Not a valid OFF header')
    n_verts, n_faces, n_dontknow = tuple([int(s) for s in file.readline().strip().split(' ')])
    file.readline()
    verts = []
    for i_vert in range(n_verts):
        verts.append([float(s) for s in file.readline().strip().split(' ')])
    faces = []
    for i_face in range(n_faces):
#        faces.append([int(s) for s in file.readline().strip().split(' ')[1:]])
        faces.append([int(s) for s in file.readline().strip().split(' ')[2:]])
    return verts, faces

def read_vtk(filename):
    import xml.etree.ElementTree as ET
    tree = ET.parse(filename)
    root = tree.getroot()
    for piece in root.findall("PolyData/Piece"):
        pointdata = piece.find("PointData")
        colors = pointdata.find('DataArray').text
        colors = [float(s) for s in colors.split()]
        points = piece.find("Points")
        vertices = points.find('DataArray').text
        vertices = [float(s) for s in vertices.split() ]
        vertices = np.array(vertices).reshape((-1,3))
        polys = piece.find("Polys")
        faces = polys.find('DataArray').text
        faces = [int(s) for s in faces.split() ]
        faces = np.array(faces).reshape((-1,3))
    return vertices, faces, colors


def format_param_pos(diameters,centers,colors):
    # build up shape_def string in a loop
    centers = centers
    colors = colors
    N = len(diameters);
    shape_def = 'sphere_union {0} '.format(N);
    if colors is None:
        # default
        colors = ["ff5984ff" for c in centers]


    for d,p,c in zip(diameters, centers, colors):
        shape_def += '{0} '.format(d);
        shape_def += '{0} {1} {2} '.format(*p);
        shape_def += '{0} '.format(c);

    return shape_def


