import numpy as np
import sys
base = sys.argv[1]

faces = np.loadtxt(base+'.face',skiprows=3,dtype=np.int32)[:,0:3]
vertices = np.loadtxt(base+'.vert',skiprows=3,dtype=np.float32,usecols=(0,1,2))/10

faces = faces - 1

# Kyte Doolittle Kd
# https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/midas/hydrophob.html
hydrophobicity = dict(ILE=4.5, VAL=4.2, LEU=3.8, PHE=2.8, CYS=2.5, MET=1.9, ALA=1.8, GLY=-0.4, THR=-0.7, SER=-0.8, TRP=-0.9,TYR=-1.3, PRO=-1.6, HIS=-3.2, GLU=-3.5, GLN=-3.5, ASP=-3.5, ASN=-3.5, LYS=-3.9, ARG=-4.5)

values = []
with open(base+'.vert') as f:
    for l in f.readlines():
        tokens = l.split()
        if len(tokens) >= 10:
            values.append(hydrophobicity[tokens[9].split('_')[1]])

with open(base+'.off','w') as out:
    out.write('4OFF\n{} {} 0\n'.format(len(vertices),len(faces)))
    out.write('\n')
    for v,val in zip(vertices,values):
        out.write('{:.5f} {:.5f} {:.5f} {:.5f}\n'.format(v[0],v[1],v[2],val))
    for f in faces:
        out.write('{} {}\n'.format(len(f),' '.join([str(int(fv)) for fv in f])))

