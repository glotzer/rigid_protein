"""Plot the performance of stampede nodes for checking"""
from matplotlib import pyplot as plt

threads_skylake = [1, 12, 24, 36, 48, 96]
threads_knights = [1, 12, 24, 36, 48, 68, 96, 136, 272]
tps_skylake = [0.00943567, 0.126123, 0.227922, 0.336131, 0.393761, 0.435489 ]
tps_knights = [0.00180732, 0.0205644, 0.039208,0.0637225 ,0.0854864 , 0.11243 , 0.117779, 0.139272,0.138747]
skylake_max = 48
knights_max = 68

fig, ax = plt.subplots(1, 1)
ax.plot(threads_skylake, tps_skylake, label = 'Skylake', color='blue')
ax.plot(threads_knights, tps_knights, label = 'Knights Landing', color='green')
ax.axvline(skylake_max, ls='--', color='blue', label = "# Skylake Nodes")
ax.axvline(knights_max, ls='--', color='green', label = "# Knights Nodes")
fig.legend()
plt.show()
