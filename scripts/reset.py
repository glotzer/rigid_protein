import signac
project = signac.get_project()
for job in project:
    with job:
        if job.document.get('denatured') is not None:
            job.doc.pop('denatured')
        if job.document.get('evaluated') is not None:
            job.doc.pop('evaluated')
