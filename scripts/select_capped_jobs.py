import signac
project = signac.get_project()

root_name = 'protomer_1_alternating_head_tail'
original_file = root_name + '.pdb'
key = 'pdb_file'

schema = project.detect_schema()
filenames = list(schema[key][str])
capped_files = [fn for fn in filenames if (root_name in fn and fn != original_file)]
print(capped_files)

jobs = project.find_jobs({key: {'$in': capped_files}})

jobs = [job for job in jobs if job.document.get('evaluated', False)]

print(len(jobs))
print(jobs[0])
