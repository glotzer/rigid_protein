# py2 compatibility
from __future__ import division, print_function, absolute_import

import numpy as np
import math

def conj(q):
    return np.array((q[0],-q[1],-q[2],-q[3]))

def rotate(q,v):
    q = np.array(q)
    v = np.array(v)
    return np.array((q[0]*q[0]-np.dot(q[1:],q[1:]))*v+2*q[0]*np.cross(q[1:],v)+2*np.dot(q[1:],v)*q[1:])

def quat_mult(a,b):
    a = np.array(a)
    b = np.array(b)
    s = a[0]*b[0] - np.dot(a[1:],b[1:])
    v = a[0]*b[1:] + b[0]*a[1:] + np.cross(a[1:],b[1:])
    return np.array((s,v[0],v[1],v[2]))

def quaternion_from_matrix(T):
    # http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
    tr = np.trace(T)
    if (tr > 0):
        S = math.sqrt(tr+1.0) * 2;
        qw = 0.25 * S;
        qx = (T[2,1] - T[1,2])/S;
        qy = (T[0,2] - T[2,0])/S;
        qz = (T[1,0] - T[0,1])/S;
    elif ((T[0,0] > T[1,1]) and (T[0,0] > T[2,2])):
        S = math.sqrt(1.0+T[0,0]-T[1,1]-T[2,2])*2;
        qw = (T[2,1] - T[1,2]) / S;
        qx = 0.25*S;
        qy = (T[0,1] + T[1,0]) / S;
        qz = (T[0,2] + T[2,0]) / S;
    elif (T[1,1] > T[2,2]):
        S = math.sqrt(1.0 + T[1,1] - T[0,0] - T[2,2])*2
        qw = (T[0,2] - T[2,0]) / S;
        qx = (T[0,1] + T[1,0]) / S;
        qy = 0.25*S;
        qz = (T[1,2] + T[2,1]) /S;
    else:
        S = math.sqrt(1.0 + T[2,2] - T[0,0] - T[1,1])*2;
        qw = (T[1,0] - T[0,1]) / S;
        qx = (T[0,2] + T[2,0]) / S;
        qy = (T[1,2] + T[2,1]) / S;
        qz = 0.25 * S;
    return np.array((qw,qx,qy,qz))

